<?php 
class CommonComponent
{
    /*function rarExtract($rarName,$appName)
    {
        $appRar = '"C:\Program Files\WinRAR\unrar.exe"';        
        $opt="x -o+";

        $source = "files/rar/$rarName";
        $destination = "../../$appName";
        $do ="$appRar $opt $source $destination";

        exec($do,$result);
        return $result;
    }*/
    
    function zipExtract($zipName,$appName)
    {
        $source = "files/zip/$zipName";
        $destination = "../../$appName";
        
        $zip = new ZipArchive;
        $res = $zip->open($source);
        if ($res === true) 
        {
            $zip->extractTo($destination);
            $zip->close();
            return 'Completed.';
        }
    }
    
    function readTextFile($filename)
    {
        if (file_exists($filename)) 
        {
            $handle = fopen($filename, "r");
            $content = fread($handle, filesize($filename));
            fclose($handle);                        
            return $content;
        }
        else
        {
            return null;
        }
    }
    
    function dirList($directory) 
    {    
        $results = array();
        $handler = opendir($directory);        
        while ($file = readdir($handler)) 
        {
            if ($file != '.' && $file != '..')
                $results[] = $file;
        }    
        closedir($handler);
        return $results;
    }
    
    function delDir($dirname)
    {
        if (is_dir($dirname))
        $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while($file = readdir($dir_handle)) 
        {
            if ($file != "." && $file != "..") 
            {
                if (!is_dir($dirname."/".$file))
                unlink($dirname."/".$file);
                else
                {
                    $a=$dirname.'/'.$file;
                    $this->delDir($a);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }
    
    function uploadfile($dirPath,$data)
    {
        $name = $data['name'];
        $tmp = $data['tmp_name'];            
        $size = $data['size'];        
        
        $ext = substr($name, strrpos($name, '.') + 1);
        $ext = strtolower($ext);
        
        $allow = array
        (
            'jpg','png','gif','bmp',
            'flv','swf','mp3','wav',
            'pdf','doc','docx','xls','xlsx','ppt','pptx','txt',
            'xml','sql',
            'rar','zip','gz'
        );
                
        if(!is_dir($dirPath))
        {
            mkdir($dirPath, 0777);
            chmod($dirPath, 0777);
        }
        
        $has = false;
        foreach($allow as $item)
        {
            if($ext == $item)
            {
                $has = true;
                break;
            }
        }
        
        if($has == true)
        {
            move_uploaded_file($tmp, $dirPath.'/'.$name);
            chmod($dirPath.'/'.$name, 0777);
            return true;
        }
        else
        {
            return false;
        }        
    }
    
     
}
?>