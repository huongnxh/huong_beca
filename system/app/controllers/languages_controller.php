<?php
class LanguagesController extends AppController 
{
	var $name = 'Languages';

	function index() 
    {
		$this->Language->recursive = 0;
        $key = $this->Session->read('Key');
        $value = $this->Session->read('Value');
        $label = $this->Session->read('Label');
        
        if(!empty($key))
        {
            $this->set('languages', $this->paginate('Language',array("Language.key like '%$key%'")));
        }
        else if(!empty($value)) 
        {
            $this->set('languages', $this->paginate('Language',array("Language.eng like '%$value%'"))); 
        }
        else
        {
            if(empty($label))
                $this->set('languages', $this->paginate());
            else
                $this->set('languages', $this->paginate('Language',array('Language.label'=>$label)));
        }
        
        $this->set('filter',$this->getTags());
        $this->set(compact('key','value','label'));
	}
    
    function getTags()
    {
        $list = $this->Language->find('all',array('order'=>array('Language.label')));
        $temp = '';
        $result = array();
        foreach($list as $item)
        {
            if($item['Language']['label'] != null)
                $tag = $item['Language']['label'];
            else
                $tag = 'Unlabel';
                
            if($tag != $temp)
            {
                $temp = $tag;
                array_push($result,$tag);
            }
                
        }
        return $result;
    }
    
    function set_filter()
    {
        if(!empty($this->data))
        {
            $this->Session->delete('Key');
            $label = $this->data['Label'];
            if($label == 'All')
            {
                $this->Session->delete('Label');
            }
            else
            {
                $this->Session->write('Label',$label);
            }
            $this->redirect(array('action'=>'index'));
        }
    }
    
    

	function view($id = null) 
    {
		if (!$id) 
        {
			$this->flashWarning(__('MsgInvalid Language', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('language', $this->Language->read(null, $id));
	}
    
    function export()
    {
        if(!empty($this->data))
        {
            $lang = $this->data['Lang'];
            $list = $this->Language->find('all');
            $content = "";
            $path = "../../../app/Locale/$lang/LC_MESSAGES/default.po";
            foreach($list as $item)
            {
                $content .= 'msgid "'.$item['Language']['key'].'"'."\n".'msgstr "'.$item['Language']["$lang"].'"'."\n";
            }               
            if ($File = new File($path, true)) {
                $File->write($content);
            } 
            $this->flashSuccess(__('MsgLanguageExported', true));
            $this->redirect(array('action' => 'index'));
        }
    }
    
    function admin_clear_cache() 
    {
        $cachePaths = array('persistent');
        $path = "../../app/tmp/cache/";
        foreach($cachePaths as $config) {
            clearCache(null, $path.$config);
        }
    } 

	function add() 
    {
		if (!empty($this->data)) 
        {
            // Check key exist
            $key = $this->Language->find('first',array('conditions'=>array('Language.key'=>$this->data['Language']['key'])));
            if(!empty($key))
            {
                $this->set('key',$key);
            }
            else
            {
                if(empty($this->data['Language']['label']))
                {
                    $this->data['Language']['label'] = 'Unlabel';
                }            
                
                $this->Language->create();
                if ($this->Language->save($this->data)) 
                {
                    $this->flashSuccess(__('MsgLanguageSaved', true));
                    $this->redirect(array('action' => 'index'));
                }
                else
                {
                    $this->flashWarning(__('MsgLanguageNotSaved', true));
                }
            }            
		}
	}

	function edit($id = null) 
    {
		if (!$id && empty($this->data)) 
        {
			$this->Session->setFlash(__('MsgInvalidlanguage', true));
			$this->redirect(array('action' => 'index'));
		}
        
		if (!empty($this->data)) 
        {
			if ($this->Language->save($this->data)) 
            {
				$this->flashSuccess(__('MsgLanguageSaved', true));
				$this->redirect(array('action' => 'index'));
			}
            else
            {
				$this->flashWarning(__('MsgLanguageNotSaved', true));
			}
		}
        
		if (empty($this->data)) 
        {
			$this->data = $this->Language->read(null, $id);
		}
	}

	function delete($id = null) 
    {
		if (!$id) 
        {
			$this->flashWarning(__('MsgInvalidLanguage', true));
			$this->redirect($this->referer());
		}
		if ($this->Language->delete($id)) 
        {
			$this->flashSuccess(__('MsgLanguageDeleted', true));
			$this->redirect($this->referer());
		}
		$this->flashWarning(__('MsgLanguageNotDeleted', true));
		$this->redirect($this->referer());
	}
    
    function search()
    {
        if(isset($this->params['form']['btnKey']) && $this->data['Key'] != 'Search by Key')
        {
            $this->Session->write('Key',$this->data['Key']);
            $this->Session->delete('Value');
        }
        else if(isset($this->params['form']['btnValue'])  && $this->data['Value'] != 'Search by Value')
        {
            $this->Session->write('Value',$this->data['Value']);
            $this->Session->delete('Key');
        }
        $this->redirect(array('action'=>'index'));
    }
    
    function show_all()
    {
        $this->Session->delete('Label');
        $this->Session->delete('Key');
        $this->Session->delete('Value');
        $this->redirect($this->referer());
    }
}
?>