<?php
class PagesController extends AppController 
{
	var $name = 'Pages';
	var $helpers = array('Html');
	var $uses = array();
    
    function index()
    {
    }
    
    function gate()
    {
        $this->layout = null;
        if(!empty($this->data))
        {
            $key = sha1($this->data['Page']['key']);
//            debug($key);die;
            if($key == '7c222fb2927d828af22f592134e8932480637c0d')
            {
                $this->Session->write('LoginKey',true);
                $this->redirect(array('controller'=>'pages','action'=>'index'));
            }
        }
    }
    
    function notebook($filename=null)
    {
        if(!empty($filename))
        {
            $filePath = 'notebook/' . $filename;
            if(@file_exists($filePath))
            {
                $content = $this->Common->readTextFile($filePath);
                $this->set('content',$content);
            }
        }
        
        $list = $this->Common->dirList('notebook');
        $this->set('list',$list);
    }
    
    function logout()
    {
        $this->Session->delete('LoginKey');
        $this->redirect(array('controller'=>'pages','action'=>'gate'));
    }

	/*function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}*/
}
