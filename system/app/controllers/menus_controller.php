<?php
class MenusController extends AppController 
{
	var $name = 'Menus';

	function index($groupID = null)
        {
        if($groupID == null) $this->redirect(array('action'=>'index',GROUP_ADMIN));
        
        $menus = array();
        $parents = $this->Menu->find('all',array('conditions'=>array('Menu.parent_id'=>null,'Menu.group'=>$groupID),'order'=>array('Menu.display_order')));
        foreach($parents as $item)
        {
            $childs = $this->Menu->find('all',array('conditions'=>array('Menu.parent_id'=>$item['Menu']['id'],'Menu.group'=>$groupID),'order'=>array('Menu.display_order')));
            
            if(count($childs) > 0) $item['Child'] = $childs;
            array_push($menus,$item);
        }
        
		$this->set('menus', $menus);
        
        $group = $this->getGroup();
        $this->set('group',$group);
	}

	function add($groupID = null) 
        {    
		if (!empty($this->data)) 
        {
			$this->Menu->create();
			if ($this->Menu->save($this->data)) 
            {
				$this->flashSuccess(__('MsgMenuSaved', true));
				$this->redirect(array('action' => 'index',$this->data['Menu']['group']));
			} 
            else 
            {
				$this->flashError(__('MsgMenuNotSaved', true));
			}
		}
        
        $group = $this->getGroup();
        $parents = $this->Menu->find('list',array('conditions'=>array('Menu.parent_id'=>null,'Menu.group'=>$groupID)));
        $this->set(compact('group','parents'));
	}
    
	function edit($id = null) 
        {
		if (!$id && empty($this->data)) 
        {
			$this->flashError(__('MsgInvalidMenu', true));
			$this->redirect(array('action' => 'index'));
		}
        
		if (!empty($this->data)) 
        {
			if ($this->Menu->save($this->data)) 
            {
				$this->flashSuccess(__('MsgMenuSaved', true));
				$this->redirect(array('action' => 'index',$this->data['Menu']['group']));
			} 
            else 
            {
				$this->flashError(__('MsgMenuMotSaved', true));
			}
		}
        
		if (empty($this->data)) 
        {
			$this->data = $this->Menu->read(null, $id);
		}
        
        $group = $this->getGroup();
        $parents = $this->Menu->find('list',array('conditions'=>array('Menu.parent_id'=>null,'Menu.group'=>$this->data['Menu']['group'],'Menu.id <>'=>$this->data['Menu']['id'])));
        $this->set(compact('group','parents'));
	}

	function delete($id = null) 
        {
		if (!$id) 
        {
			$this->flashError(__('MsgInvalidMenu', true));
			$this->redirect($this->referer());
		}
        
		if ($this->Menu->delete($id)) 
        {
			$this->flashSuccess(__('MsgMenuDeleted', true));
			$this->redirect($this->referer());
		}
        
		$this->flashError(__('MsgMenuNotDeleted', true));
		$this->redirect($this->referer());
	}
    
    function set_active($id,$bool = true)
    {
        $this->Menu->id = $id;
        $this->Menu->saveField('display',$bool);
        $this->redirect($this->referer());
    }
    
    function sort_parent()
    {
        if(!empty($this->data))
        {
            //debug($this->data);die;
            $data = array();
            $i = 1;
            foreach($this->data['Order'] as $id)
            {
                $row = array
                (
                    'id'=>$id,
                    'display_order'=>$i
                );
                array_push($data,$row);
                ++$i;
            }
            if($this->Menu->saveAll($data))
            {
                $this->flashSuccess('MsgSortParentSuccessful');
            }
            $this->redirect($this->referer());
        }
    }
    
    function sort_child($menuParentID = null)
    {
        $parent = $this->Menu->find('first',array('conditions'=>array('Menu.id'=>$menuParentID)));
        if(!empty($this->data))
        {
            $data = array();
            $i = 1;
            foreach($this->data['Order'] as $id)
            {
                $row = array
                (
                    'id'=>$id,
                    'display_order'=>$i
                );
                array_push($data,$row);
                ++$i;
            }
            if($this->Menu->saveAll($data))
            {
                $this->flashSuccess('MsgSortParentSuccessful');
            }
            $this->redirect(array('controller'=>'menus','action'=>'index',$parent['Menu']['group']));
        }
        
        $menus = $this->Menu->find('all',array('conditions'=>array('Menu.parent_id'=>$menuParentID)));
        $this->set(compact('menus','parent'));
    }
}
