<?php
class GroupAuthsController extends AppController 
{
	var $name = 'GroupAuths';

	function index($groupID = null) 
    {
        if($groupID == null) $this->redirect(array('action'=>'index',GROUP_ADMIN));
        $list = $this->GroupAuth->find('all',array('conditions'=>array('GroupAuth.group'=>$groupID),'order'=>array('GroupAuth.controller'=>'asc','GroupAuth.action'=>'asc')));
        
        $groupAuths = array();
        foreach($list as $item)
        {
            if(!isset($groupAuths[$item['GroupAuth']['controller']])) $groupAuths[$item['GroupAuth']['controller']] = array();
            array_push($groupAuths[$item['GroupAuth']['controller']],array('id'=>$item['GroupAuth']['id'],'action'=>$item['GroupAuth']['action']));
        }
        
        $group = $this->getGroup();
		$this->set(compact('groupAuths', 'group'));
	}

	function add() 
    {
		if (!empty($this->data)) 
        {
			$this->GroupAuth->create();
			if ($this->GroupAuth->save($this->data)) 
            {
				$this->flashSuccess(__('MsgGroupAuthSaved', true));
				$this->redirect($this->referer());
			}
            else
            {
				$this->redirect($this->referer());
			}
		}
	}
    
    function delete_action($id = null) 
    {
        if ($this->GroupAuth->delete($id)) 
        {
            $this->flashSuccess(__('MsgActionDeleted', true));
            $this->redirect($this->referer());
        }
        $this->flashWarning(__('MsgActionDeleted', true));
        $this->redirect($this->referer());
    }

	function delete_controller($groupID,$controller) 
    {
		if($this->GroupAuth->deleteAll(array('GroupAuth.group'=>$groupID,'GroupAuth.controller'=>$controller)))
        {
			$this->flashSuccess(__('MsgControllerDeleted', true));
			$this->redirect($this->referer());
		}
		$this->flashWarning(__('MsgControllerNotDeleted', true));
		$this->redirect($this->referer());
	}
}
?>