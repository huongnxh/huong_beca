<?php 
    $groupID = $this->params['pass'][0];
 ?>
<div id="container">
    <div class="shell">
        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("MenuManagement") ?></b>
            <span>&gt;</span>
            <?php __("MenuList") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>

        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2 class="left">
                            <?php echo $group[$groupID] ?>
                        </h2>
                        <div class="right">
                            <?php foreach($group as $id => $name): ?>
                                <a class="button" href="<?php echo Helper::url("../menus/index/".$id) ?>"><span><?php echo $name ?></span></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- End Box Head -->    

                    <!-- Table -->
                    <?php echo $form->create('Menu',array('action'=>'sort_parent')) ?>
                    <div class="table">
                        <?php if(isset($menus) && count($menus) > 0): ?>
                            <table id="tblSortable" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="sort-disabled">
                                    <th><?php __("Name") ?></th>
                                    <th width="20%"><?php __("Controller") ?></th>
                                    <th width="20%"><?php __("Action") ?></th>
                                    <th width="7%"><?php __("Display") ?></th>
                                    <th width="25%" style="text-align:center;"><?php __("Action") ?></th>
                                </tr>
                            <?php foreach($menus as $item): ?>
                                <tr style="color:#ba4c32;">
                                    <td>
                                        <input type="hidden" name = "data[Order][]" value="<?php echo $item['Menu']['id'] ?>" />
                                        <?php echo $item['Menu']['name'] ?>
                                    </td>
                                    <td><?php echo $item['Menu']['controller'] ?></td>
                                    <td><?php echo $item['Menu']['action'] ?></td>
                                    <td>
                                        <?php
                                            if($item['Menu']['display'])
                                                echo $html->link($html->image("system/active.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$item['Menu']['id'],0),array('escape'=>false));
                                            else
                                                echo $html->link($html->image("system/lock.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$item['Menu']['id'],1),array('escape'=>false));
                                        ?>
                                    </td>
                                    <td align="center">
                                        <?php echo $html->link(__('Edit',true) , array('action' => 'edit', $item['Menu']['id'],$groupID), array('class'=>'ico edit')); ?>
                                        <?php echo $html->link(__('Delete',true), array('action' => 'delete', $item['Menu']['id']), array('class'=>'ico del'), sprintf(__('MsgConfirmDelete', true), $item['Menu']['id'])); ?>
                                        <?php echo $html->link(__('Sort',true) , array('action' => 'sort_child', $item['Menu']['id']),array('class'=>'sortchild','style'=>'display:none')); ?>
                                    </td>
                                </tr>
                                <!-- Render Child { -->
                                <?php if(isset($item['Child'])): ?>
                                <?php foreach($item['Child'] as $subItem): ?>
                                <tr class="sort-disabled">
                                    <td><?php echo $subItem['Menu']['name'] ?></td>
                                    <td><?php echo $subItem['Menu']['controller'] ?></td>
                                    <td><?php echo $subItem['Menu']['action'] ?></td>
                                    <td>
                                        <?php
                                            if($subItem['Menu']['display'])
                                                echo $html->link($html->image("system/active.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$subItem['Menu']['id'],0),array('escape'=>false));
                                            else
                                                echo $html->link($html->image("system/lock.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$subItem['Menu']['id'],1),array('escape'=>false));
                                        ?>
                                    </td>
                                    <td align="center">
                                        <?php echo $html->link(__('Edit',true) , array('action' => 'edit', $subItem['Menu']['id'],$groupID), array('class'=>'ico edit')); ?>
                                        <?php echo $html->link(__('Delete',true), array('action' => 'delete', $subItem['Menu']['id']), array('class'=>'ico del'), sprintf(__('MsgConfirmDelete', true), $subItem['Menu']['id'])); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <!-- Render Child } -->
                            <?php endforeach; ?>
                            </table>
                        <?php else: ?>
                            <table cellpadding="3" cellspacing="5" width="100%">
                                <tr>
                                    <td align="center"><?php __("NoData") ?></td>
                                </tr>
                            </table>
                        <?php endif; ?>
                        
                        <!-- Pagging -->
                        <div class="pagging">
                            <div class="left"></div>
                            <div class="right">
                                    <input id = "btnSubmitSort" type="submit" class="button" value="<?php __("Sort") ?>" style="display:none;" />
                            </div>
                        </div>
                        <!-- End Pagging -->
                        
                    </div>
                    </form>
                    <!-- Table -->
                    
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/menus/right',array('groupID'=>$groupID)) ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>

<script>
$(function() 
{
    $("#tblSortable tbody").sortable({
        items: ">*:not(.sort-disabled)",
        stop: function(event, ui)
    { 
        $("#btnSubmitSort").css('display','inline');
        $(".sort-disabled").hide();
        $(".sortchild").show();
    }});
    $("#tblSortable tbody").sortable({items: ">*:not(.sort-disabled)"}).disableSelection();
});
</script>