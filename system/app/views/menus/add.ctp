<?php $groupID = $this->params['pass'][0];?>

<div id="container">
    <div class="shell">        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("MenuManagement") ?></b>
            <span>&gt;</span>
            <?php echo $html->link(__('MenuList',true),array('controller'=>'menus','action'=>'index',$groupID));  ?>
            <span>&gt;</span>
            <?php __("MenuAdd") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>
        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2><?php echo $group[$groupID] ?></h2>
                    </div>
                    <!-- End Box Head -->
                    
                        <!-- Form -->
                        <?php echo $this->Form->create('Menu');?>
                        <input type="hidden" name = "data[Menu][group]" value="<?php echo $groupID ?>" />
                        <div class="form">
                        
                                <table cellpadding="3" cellspacing="5" class="list" width="100%">
                                    <tr>
                                        <td width="30%" align="right"><?php __("Parent") ?></td>
                                        <td align="left">
                                            <select name="data[Menu][parent_id]" class="field">                
                                                <option value=""> -- <?php __("SelectParent") ?> -- </option>
                                                <?php foreach($parents as $key => $text): ?>
                                                    <option value="<?php echo $key ?>"><?php echo $text ?></option>
                                                <?php endforeach; ?>                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"><?php __("Name") ?></td>
                                        <td align="left"><?php echo $this->Form->input('name',array('class'=>'field','label'=>false)); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="vertical-align:top"><?php __("Description") ?></td>
                                        <td align="left"><?php echo $this->Form->input('description',array('class'=>'field','label'=>false,'rows'=>2)); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><?php __("Controller") ?></td>
                                        <td align="left"><?php echo $this->Form->input('controller',array('class'=>'field','label'=>false)); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><?php __("Action") ?></td>
                                        <td align="left"><?php echo $this->Form->input('action',array('class'=>'field','label'=>false)); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><?php __("Display") ?></td>
                                        <td align="left"><?php echo $this->Form->input('display',array('class'=>'field','label'=>false)); ?></td>
                                    </tr>
                                </table>                             
                        </div>
                        <!-- End Form -->
                        
                        <!-- Form Buttons -->
                        <div class="buttons">
                            <input type="submit" class="button" value="<?php __("Save") ?>" />
                            <input type = "button" class="button" onclick="location.href='<?php echo $html->url('/menus/index/'.$groupID); ?>';" value="<?php __("Back") ?>" />
                        </div>
                        <!-- End Form Buttons -->
                    </form>
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/menus/right',array('groupID'=>$groupID)) ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>