<?php $groupID = $parent['Menu']['group'] ?>
<div id="container">
    <div class="shell">
        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("MenuManagement") ?></b>
            <span>&gt;</span>
            <?php echo $html->link(__('MenuList',true),array('controller'=>'menus','action'=>'index',$groupID));  ?>
            <span>&gt;</span>
            <?php __("SortChild") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>

        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2 class="left">
                            <?php echo $parent['Menu']['name'] ?>
                        </h2>
                        <div class="right">
                            <?php __("Drag & Drop to sort") ?>
                        </div>
                    </div>
                    <!-- End Box Head -->    

                    <!-- Table -->
                    <?php echo $form->create('Menu',array('action'=>'sort_child/'.$parent['Menu']['id'])) ?>
                    <div class="table">
                        <?php if(isset($menus) && count($menus) > 0): ?>
                            <table id="tblSortable" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th><?php __("Name") ?></th>
                                    <th width="20%"><?php __("Controller") ?></th>
                                    <th width="20%"><?php __("Action") ?></th>
                                    <th width="7%"><?php __("Display") ?></th>
                                    <th width="25%" style="text-align:center;"><?php __("Action") ?></th>
                                </tr>
                            <?php foreach($menus as $item): ?>
                                <tr>
                                    <td>
                                        <input type="hidden" name = "data[Order][]" value="<?php echo $item['Menu']['id'] ?>" />
                                        <?php echo $item['Menu']['name'] ?>
                                    </td>
                                    <td><?php echo $item['Menu']['controller'] ?></td>
                                    <td><?php echo $item['Menu']['action'] ?></td>
                                    <td>
                                        <?php
                                            if($item['Menu']['display'])
                                                echo $html->link($html->image("system/active.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$item['Menu']['id'],0),array('escape'=>false));
                                            else
                                                echo $html->link($html->image("system/lock.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$item['Menu']['id'],1),array('escape'=>false));
                                        ?>
                                    </td>
                                    <td align="center">
                                        <?php echo $html->link(__('Edit',true) , array('action' => 'edit', $item['Menu']['id'],$groupID), array('class'=>'ico edit')); ?>
                                        <?php echo $html->link(__('Delete',true), array('action' => 'delete', $item['Menu']['id']), array('class'=>'ico del'), sprintf(__('MsgConfirmDelete', true), $item['Menu']['id'])); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </table>
                        <?php else: ?>
                            <table cellpadding="3" cellspacing="5" width="100%">
                                <tr>
                                    <td align="center"><?php __("NoData") ?></td>
                                </tr>
                            </table>
                        <?php endif; ?>
                        
                        <!-- Pagging -->
                        <div class="pagging">
                            <div class="left"></div>
                            <div class="right">
                                    <input id = "btnSubmitSort" type="submit" class="button" value="<?php __("Sort") ?>" />
                            </div>
                        </div>
                        <!-- End Pagging -->
                        
                    </div>
                    </form>
                    <!-- Table -->
                    
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/menus/right',array('groupID'=>$groupID)) ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>

<script>
$(function() 
{
    $("#tblSortable tbody").sortable({items: ">*:not(.sort-disabled)",stop: function(event, ui) 
    {
        $(".sort-disabled").hide();
    }});
    $("#tblSortable tbody").sortable({items: ">*:not(.sort-disabled)"}).disableSelection();
});
</script>