<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php __('dCore'); ?>
        <?php echo $title_for_layout; ?>
    </title>
    
    <?php echo $javascript->link('jquery.js'); ?>
    <?php echo $javascript->link('jquery-ui') ?>
    <?php echo $html->css('jquery/ui-lightness/jquery-ui'); ?>
    
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('default');
        echo $scripts_for_layout;
    ?>    
</head>
<body>

<?php echo $this->element('default/header') ?>

<!-- Container -->
<?php echo $content_for_layout; ?>
<!-- End Container -->

<?php echo $this->element('default/footer') ?>

<center>
    <?php echo $this->element('sql_dump'); ?>
</center>
    
</body>
</html>
            