<!-- Header -->
<div id="header">
    <div class="shell">
        <!-- Logo + Top Nav -->
        <div id="top">
            <h1><a href="#">dCore</a></h1>
            <div id="top-navigation">
                Welcome <a href="#"><strong>Administrator</strong></a>
                <span>|</span>
                <?php echo $html->link(__('Logout',true),array('controller'=>'pages','action'=>'logout'));  ?>
            </div>
        </div>
        <!-- End Logo + Top Nav -->
        
        <!-- Main Nav -->
        <div id="navigation">            
            <?php 
                $curController = $this->params['controller'];
                $curAction = $this->params['action'];
             ?>
            <ul>
                <li><a href="<?php echo Helper::url("../pages/index") ?>" <?php if($curController=='pages' && $curAction=='index') echo 'class="active"' ?> ><span><?php __("Home") ?></span></a></li>
                <?php if(isset($topMenus) && count($topMenus) > 0): ?>                
                <?php foreach($topMenus as $item): ?>
                    <li><a href="<?php echo Helper::url("../".$item['controller']."/".$item['action']) ?>" <?php if($curController==$item['controller']) echo 'class="active"' ?> ><span><?php echo __($item['name'],true) ?></span></a></li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <!-- End Main Nav -->
    </div>
</div>
<!-- End Header -->