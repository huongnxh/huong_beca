<style>
.text-label 
{
    color: #cdcdcd;
    font-weight: italic;
}
</style>

<!-- Sidebar -->
<div id="sidebar">
    
    <!-- Box -->
    <div class="box">
        
        <!-- Box Head -->
        <div class="box-head">
            <h2>Management</h2>
        </div>
        <!-- End Box Head-->
        
        <div class="box-content">
            <a class="add-button" href="<?php echo Helper::url("../languages/add") ?>"><span><?php __("New") ?></span></a>
            <div class="cl">&nbsp;</div><br>            
            <table>
                <tr>
                    <td align="right"><b><?php __("Filter") ?></b> &nbsp;</td>
                    <td><?php echo $form->create('Language',array('action'=>'set_filter'),array('div'=>false)) ?>
                        <select name="data[Label]" class="field" style = "width:100px"  />
                            <option value="All">All</option>
                            <?php foreach($filter as $item): ?>
                                <option value="<?php echo $item ?>" <?php if($label == $item) echo 'selected="selected"' ?>><?php echo $item ?></option>
                            <?php endforeach; ?>                
                        </select>
                        <input type="submit" value="<?php __("OK") ?>" class="button" />
                        </form></td>
                </tr>
                    <td align="right"><b><?php __("Export") ?></b> &nbsp;</td>
                    <td>
                        <?php echo $form->create('Language',array('action'=>'export'),array('div'=>'')) ?>
                        <select name="data[Lang]" class="field" style = "width:100px" />
                            <option value="eng">Eng &nbsp;</option>
                            <option value="vie">Vie &nbsp;</option>
                        </select>
                        <input type="submit" value="<?php __("OK") ?>" class="button" />
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div class="box">
            <!-- Box Head -->
            <div class="box-head">
                <h2><?php __("Search") ?></h2>
            </div>
            <!-- End Box Head-->
            
            <div class="box-content">
                <?php echo $form->create('Language',array('action'=>'search')) ?>
                    <table cellpadding="3" cellspacing="5">
                        <tr>
                            <td><input type = "text" name="data[Key]" value="" class="defaultText field" title="Search by Key" style="width:120px" /></td>
                            <td><input type="submit" value="<?php __("Search") ?>" class="button" name="btnKey" /></td>
                        </tr>
                        <tr>
                            <td><input type = "text" name="data[Value]" value="" class="defaultText field" title="Search by Value" style="width:120px" /></td>
                            <td><input type="submit" value="<?php __("Search") ?>" class="button" name="btnValue" /></td>
                        </tr>
                    </table>
                </form>
                                
                <!-- <?php echo $form->create('Language',array('action'=>'search')) ?>
                    <table cellpadding="3" cellspacing="5">
                        <tr>
                            <td><input type = "text" name="data[Jpn]" value="" class="defaultText field" title="Search Jpn" style="width:120px" /></td>
                            <td><input type="submit" value="<?php __("Search") ?>" class="button" /></td>
                        </tr>
                    </table>
                </form> -->
            </div>
        </div>
    
    
    <!-- End Box -->
</div>
<!-- End Sidebar -->


<script language="javascript">
<!--
$(document).ready(function()
{
    $('.defaultText').each(function(){
 
    this.value = $(this).attr('title');
    $(this).addClass('text-label');
 
    $(this).focus(function(){
        if(this.value == $(this).attr('title')) {
            this.value = '';
            $(this).removeClass('text-label');
        }
    });
 
    $(this).blur(function(){
        if(this.value == '') {
            this.value = $(this).attr('title');
            $(this).addClass('text-label');
        }
    });
});        
});
//-->
</script>