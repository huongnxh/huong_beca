<!-- Sidebar -->
<div id="sidebar">
    
    <!-- Box -->
    <div class="box">
        
        <!-- Box Head -->
        <div class="box-head">
            <h2>Management</h2>
        </div>
        <!-- End Box Head-->
                
        <div class="box-content">
            <?php echo $form->create('GroupAuth',array('action'=>'add')) ?>
            <input type="hidden" name = "data[group]" value="<?php echo $groupID ?>" />
            <table cellpadding="3" cellspacing="5" width="100%">
                <tr>
                    <td align="right"><?php __("Controller") ?></td>
                    <td align="left"><input type="text" name="data[controller]" size="15" /></td>
                </tr>
                <tr>
                    <td align="right"><?php __("Action") ?></td>
                    <td align="left"><input type="text" name="data[action]" size="15" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="left">
                        <input class="button" type="submit" value="<?php __("Add Auth") ?>" />
                    </td>
                </tr>
            </table>
            </form>
            
            <div class="cl">&nbsp;</div>
        </div>
    </div>
    <!-- End Box -->
</div>
<!-- End Sidebar -->