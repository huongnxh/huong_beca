<!-- Sidebar -->
<div id="sidebar">
    <!-- Box -->
    <div class="box">        
        <!-- Box Head -->
        <div class="box-head">
            <h2>Management</h2>
        </div>
        <!-- End Box Head-->
        
        <div class="box-content">
            <a class="add-button" href="<?php echo Helper::url("../menus/add/".$groupID) ?>"><span><?php __("New Menu") ?></span></a>
            <?php if($this->params['action'] != 'index'): ?>
                <br><br><a class="normal-button" href="<?php echo Helper::url("../menus/index/".$groupID) ?>"><span><?php __("Back") ?></span></a>
            <?php endif; ?>
            <div class="cl">&nbsp;</div>
			
        </div>
    </div>
    <!-- End Box -->
</div>
<!-- End Sidebar -->