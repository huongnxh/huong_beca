<!-- Message Error -->
<div id = "msgDiv" class="msg msg-error">
    <p><strong><?php echo $message ?></strong></p>
    <a href="#" class="close" onclick="$('#msgDiv').hide();"><?php __("Close") ?></a>
</div>
<!-- End Message Error -->