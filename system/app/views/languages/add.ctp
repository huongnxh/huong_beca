<div id="container">
    <div class="shell">        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("LanguageManagement") ?></b>
            <span>&gt;</span>
            <?php echo $html->link(__('LanguageList',true),array('controller'=>'languages','action'=>'index'));  ?>
            <span>&gt;</span>
            <?php __("LanguageAdd") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>
        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2></h2>
                        <div class="right">
                            <?php if(isset($key)): ?>
                                <?php __("Key exist") ?> 
                                (
                                    <?php __("Eng:") ?> <?php echo $key['Language']['eng'] ?> - 
                                    <?php __("Vie:") ?> <?php echo $key['Language']['vie'] ?>
                                )
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- End Box Head -->
                    
                        <!-- Form -->
                        <?php echo $this->Form->create('Language');?>
                        <div class="form">
                            <table cellpadding="3" cellspacing="5" width="100%">
                                <tr>
                                    <td width="30%" align="right"><?php __("Key") ?></td>
                                    <td align="left"><?php echo $form->input('key',array('class'=>'field','label'=>'','div'=>'')); ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><?php __("English") ?></td>
                                    <td align="left"><?php echo $form->input('eng',array('class'=>'field','label'=>'','div'=>'')); ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><?php __("Vietnamesee") ?></td>
                                    <td align="left"><?php echo $form->input('vie',array('class'=>'field','label'=>'','div'=>'')); ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><?php __("Label") ?></td>
                                    <td align="left">
                                        <?php echo $form->input('label',array('class'=>'field','label'=>'','div'=>'')); ?></td>
                                </tr>
                            </table>
                        </div>
                        <!-- End Form -->
                        
                        <!-- Form Buttons -->
                        <div class="buttons">
                            <input type="submit" class="button" value="<?php __("Save") ?>" />
                            <input type = "button" class="button" onclick="location.href='<?php echo $html->url('/languages/index'); ?>';" value="<?php __("Back") ?>" />
                        </div>
                        <!-- End Form Buttons -->
                    </form>
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/languages/right') ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>