<div id="container">
    <div class="shell">
        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("LanguageManagement") ?></b>
            <span>&gt;</span>
            <?php __("LanguageList") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>

        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2 class="left"></h2>
                        <div class="right">
                            <?php if(!empty($key) || !empty($label) || !empty($value)): ?>
                                <?php echo $html->link(__('ShowAll',true),array('action'=>'show_all'),array('class'=>'button'));  ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- End Box Head -->    

                    <!-- Table -->
                    <div class="table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th><?php echo $this->Paginator->sort('key');?></th>
                                <th width="20%"><?php echo $this->Paginator->sort('eng');?></th>
                                <th width="20%"><?php echo $this->Paginator->sort('vie');?></th>
                                <th align="center" width="20%" style="text-align:center;"><?php __('Actions');?></th>
                            </tr>
                            <?php
                            $i = 0;
                            foreach ($languages as $language):
                                $class = null;
                                if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                                }
                            ?>
                            <tr<?php echo $class;?>>
                                <td><?php echo $language['Language']['key']; ?>&nbsp;</td>
                                <td><?php echo $language['Language']['eng']; ?>&nbsp;</td>
                                <td><?php echo $language['Language']['vie']; ?>&nbsp;</td>
                                <td>
                                    <?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $language['Language']['id']),array('class'=>'ico edit')); ?>
                                    <?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $language['Language']['id']), array('class'=>'ico del'), sprintf(__('MsgConfirmDelete', true), $language['Language']['id'])); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                            </table>
                        
                        <!-- Pagging -->
                        <div class="pagging">
                            <div class="left"></div>
                            <div class="right">
                                <?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled','style'=>'display:inline'));?>
                                  <?php echo $paginator->numbers(array('separator'=>''));?>
                                <?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled','style'=>'display:inline'));?>
                            </div>
                        </div>
                        <!-- End Pagging -->
                        
                    </div>
                    <!-- Table -->
                    
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/languages/right') ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>