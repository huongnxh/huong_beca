<?php 
    $groupID = $this->params['pass'][0];
 ?>
<div id="container">
    <div class="shell">
        
        <!-- Small Nav -->
        <div class="small-nav">
            <b><?php __("GroupAuthManagement") ?></b>
            <span>&gt;</span>
            <?php __("GroupAuthList") ?>
        </div>
        <!-- End Small Nav -->
        
        <?php echo $this->Session->flash(); ?>
        
        <!-- Main -->
        <div id="main">
            <div class="cl">&nbsp;</div>
            
            <!-- Content -->
            <div id="content">
                
                <!-- Box -->
                <div class="box">
                    <!-- Box Head -->
                    <div class="box-head">
                        <h2 class="left"><?php echo $group[$groupID] ?></h2>
                        <div class="right">
                            <?php foreach($group as $id => $name): ?>
                                <a class="button" href="<?php echo Helper::url("../group_auths/index/".$id) ?>"><span><?php echo $name ?></span></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- End Box Head -->    

                    <!-- Table -->
                    <div class="table">
                        <?php if(isset($groupAuths) && count($groupAuths) > 0): ?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th width="20%"><?php __("Controller") ?></th>
                                    <th><?php __("Action") ?></th>
                                    <th class="ac" align="center"><?php __("Action") ?></th>
                                </tr>
                                <?php foreach($groupAuths as $controller => $item): ?>
                                <tr>
                                    <td><?php echo $controller ?></td>
                                    <td>
                                        <?php foreach($item as $subItem): ?>
                                            <?php echo $html->link($subItem['action'],array('action'=>'delete_action',$subItem['id']),null,__('MsgConfirmDelete',true));  ?></th>
                                        <?php endforeach; ?>
                                        
                                    <td width="10%" align="center"><?php echo $html->link(__('Delete',true), array('action' => 'delete_controller', $groupID, $controller), array('class'=>'ico del'), __('MsgConfirmDelete', true)); ?></th>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php else: ?>
                            <table cellpadding="3" cellspacing="5" width="100%">
                                <tr>
                                    <td align="center"><?php __("NoData") ?></td>
                                </tr>
                            </table>
                        <?php endif; ?>
                        
                        
                        <!-- Pagging -->
                        <div class="pagging">
                            <div class="left"></div>
                            <div class="right"></div>
                        </div>
                        <!-- End Pagging -->
                        
                    </div>
                    <!-- Table -->
                    
                </div>
                <!-- End Box -->

            </div>
            <!-- End Content -->
            
            <?php echo $this->element('default/group_auths/right',array('groupID'=>$groupID)) ?>
            
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Main -->
    </div>
</div>