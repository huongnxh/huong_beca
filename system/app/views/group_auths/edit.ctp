<div class="container clearfix">
<div class="title">
    <p><?php __('Group Auth');?></p>
        <h6></h6>
        <h5>&nbsp;<?php __('Edit');?></h5>
</div>    
 <?php echo $this->Session->flash(); ?>
<!--left start-->
<!--left end-->

<!--midle start-->        
<div class="midle">                    
    <div class="under_title">                
        <p>
            <?php echo $html->link(__('ListGroup Auths', true), array('action' => 'index'));?>        </p>
        <div class="search">
        </div>
        <div class="cl"></div>
    </div>
    <div class="center">
        <?php echo $form->create('GroupAuth');?>
        <table>
            <tr class="title_table">
                <td colspan="2"></td>
            </tr>

            <?php echo $form->input('id',array('label'=>'','div'=>'')); ?>			<tr>
				<td align='right'><?php __('Group') ?></td>
				<td align='left'><?php echo $form->input('group',array('label'=>'','div'=>'')); ?></td>
			</tr>
			<tr>
				<td align='right'><?php __('Controller') ?></td>
				<td align='left'><?php echo $form->input('controller',array('label'=>'','div'=>'')); ?></td>
			</tr>
			<tr>
				<td align='right'><?php __('Action') ?></td>
				<td align='left'><?php echo $form->input('action',array('label'=>'','div'=>'')); ?></td>
			</tr>
            <tr>
                <td></td>
                <td align="left">
                    <input type="submit" value="Submit" class="btn" />                    
                    <input type = "button" class="btn" onclick="location.href='<?php echo $html->url('/admin/groupauths'); ?>';" value="<?php __("Back") ?>" />
                </td>
            </tr>
            <tr class="title_table_bottom">
                <td colspan="2">
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
</div>