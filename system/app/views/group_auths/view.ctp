<div class="container clearfix">
<div class="title">
    <p><?php __('Group Auth');?></p>
        <h6></h6>
        <h5>&nbsp;<?php __('View') ?></h5>
</div>    
<?php echo $this->Session->flash(); ?>
<!--left start-->
<!--left end-->

<!--midle start-->        
<div class="midle">                    
    <div class="under_title">                
        <p>
            <?php echo $html->link(__('ListGroup Auths', true), array('action' => 'index')); ?>        </p>
        <div class="search">
        </div>
        <div class="cl"></div>
    </div>
    <div class="center">
        <table>
            <tr class="title_table">
                <td colspan="2"></td>
            </tr>
            			<tr>
				<td align = 'right'><?php __('Id') ?></td>
				<td align = 'left'><?php echo $groupAuth['GroupAuth']['id']; ?></td>
			</tr>
			<tr>
				<td align = 'right'><?php __('Group') ?></td>
				<td align = 'left'><?php echo $groupAuth['GroupAuth']['group']; ?></td>
			</tr>
			<tr>
				<td align = 'right'><?php __('Controller') ?></td>
				<td align = 'left'><?php echo $groupAuth['GroupAuth']['controller']; ?></td>
			</tr>
			<tr>
				<td align = 'right'><?php __('Action') ?></td>
				<td align = 'left'><?php echo $groupAuth['GroupAuth']['action']; ?></td>
			</tr>
            <tr class="title_table_bottom">
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
</div>
</div>