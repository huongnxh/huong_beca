<?php
class AppController extends Controller 
{
    //var $helpers = array('Html','Javascript');
    var $helpers = array('Html', 'Javascript','Session','Form'); 
    var $components = array('Common','Session');
    function beforeFilter()
    {        
        parent::beforeFilter();
        $this->checkLogin();
        
        $topMenus = $this->getMenu();
        $this->set('topMenus',$topMenus);
    }
    
    function checkLogin()
    {
        if($this->params['controller'] == 'pages' && $this->params['action'] == 'gate')
        {            
            // Allow
        }
        else if($this->params['controller'] == 'pages' && $this->params['action'] == 'logout')
        {            
            // Allow
        }
        else
        {
            $loginKey = $this->Session->read('LoginKey');
            if(!isset($loginKey))
            {
                $this->redirect(array('controller'=>'pages','action'=>'gate'));
            }
            else
            {
                // Allow
            }
        }
    }
    
    function getMenu()
    {
        $list = $this->Common->dirList('menus');
        $menus = array();
        foreach($list as $item)
        {
            if($item == '.svn') continue;
            $content = $this->Common->readTextFile('menus/'.$item);
            if(!empty($content))
            {
                array_push($menus,json_decode($content,true));
            }
        }
        sort($menus);
        return $menus;
    }
    
    function getGroup()
    {
        $group = array
        (
            GROUP_ADMIN => __('GroupAdmin',true),
            GROUP_MEMBER => __('GroupMember',true),
//            GROUP_QA => __('GroupQA',true),
//            GROUP_SUPERVISOR => __('GroupSupervisor',true),
//            GROUP_SUPPLIER => __('GroupSupplier',true),
//            GROUP_FARMER => __('GroupFarmer',true),
        );
        return $group;
    }
    
    # sets up successful session flash message for view
    function flashSuccess($msg, $url = null)
    {
        $this->Session->setFlash($msg, 'flash'.DS.'success');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }

    # sets up warning session flash message for view
    function flashError($msg, $url = null)
    {
        $this->Session->setFlash($msg, 'flash'.DS.'error');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }
}
