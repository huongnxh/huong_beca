<!--right-->
<div class="right">
    <!--gallery-->
    <div class="gallery_blog">
        <table width="100%">
            <tr>
                <td width="45%" align="center"><a href="<?php echo $this->Html->url(array('controller'=>'galleryGroups', 'action'=>'index'))?>"><?php echo $this->Html->image('frontend/gallery_medium.png')?></a></td>
                    <?php 
                        if(isset($galleryGroups) && count($galleryGroups)>0){
                            echo '<td width="55%" class="gallery_list">';
                            foreach($galleryGroups as $galleryGroup){
                                echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'118/'. $galleryGroup['Gallery']['image'], array('class'=>'gallery_thumnails')), array('controller'=>'galleryGroups', 'action'=>'index'), array('escape'=>false));
                            }
                            echo '</td>';
                        }
                    ?>
                    
            </tr>
        </table>
    </div>
    <!--gallery-->
    <!--tin tuc-->
    <div class="tintuc_blog">
        <?php if(isset($news) && $news): $first_new = $news[0]['Post']['id']?>
            <table width="100%">
                <tr>
                    <td width="50%" >
                        <?php echo $this->Html->image('frontend/new_text.png')?>
                        <p class="orange_text"><span style="font-size:30px; line-height:35px;"><?php echo $news[0]['Post']['title']?></span><span></span></p>
                        <br>
                        <p align="justify"><?php echo $this->Text->truncate($news[0]['Post']['content'], 200, array('ending'=>'...', 'exact'=>'true', 'html'=>'true'))?></p>
                        <br>
                        <p align="right"><?php echo $this->Html->link('Xem', array('controller'=>'posts', 'action'=>'view', $news[0]['Post']['id']), array('class'=>'button2'))?></p>
                    </td>
                    <td width="50%" class="gallery_list">
                        <?php echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'329/'.$news[0]['Post']['image'], array('style'=>'width: 100%')), array('controller'=>'posts', 'action'=>'view', $news[0]['Post']['id']), array('escape'=>false))?>
                    </td>
                </tr>
            </table>
            <div class="line_green"></div>
            <?php 
                foreach($news as $new){
					if($new['Post']['id'] == $first_new){continue;}
                    echo '<div class="list_tintuc">';
                    echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'88/'.$new['Post']['image'], array('style'=>'width: 40%')), array('controller'=>'posts', 'action'=>'view', $new['Post']['id']), array('escape'=>false));
                    echo '<p>'.$this->Html->link($new['Post']['title'], array('controller'=>'posts', 'action'=>'view', $new['Post']['id'])).'</p>';
                    echo '</div>';
                }
            ?> 
            <div class="cl"></div>
        <?php endif;?>
    </div>
    <!--tin tuc-->
</div>
<!--right-->
