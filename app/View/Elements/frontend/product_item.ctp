<div class="productlist fix_relative">
    <a href="<?php echo $this->Html->url(array('controller'=>'products', 'action'=>'view', $item['Product']['id']))?>">
        <?php
            if(!$item['Product']['is_new'])
                echo $this->Html->image(Configure::read('settings.imageDir') .'218/'. $item['Product']['image']);
            else
                echo $this->Html->image(Configure::read('settings.imageDir') .'218/'. $item['Product']['image']).$this->Html->image('frontend/icon_new.png', array('class'=>'icon_new'));
            
        ?>
    </a>
    <h3><?php echo $this->Html->link($item['Product']['name'], array('controller'=>'products', 'action'=>'view', $item['Product']['id']))?></h3>
    <p align="justify">
        <?php echo $this->Text->truncate(strip_tags($item['Product']['description']), 30, array('ending'=>'...', 'excat'=>true))?>
    </p>
    <p align="right" style="padding-top:10px;">
        <?php 
            if($item['Product']['display_price']){
                echo $this->Html->link($item['Product']['price'], array('controller'=>'products', 'action'=>'view', $item['Product']['id']), array('class'=>'button_gia'));
            }
			echo '&nbsp;';
			echo $this->Html->link('Xem chi tiết', array('controller'=>'products', 'action'=>'view', $item['Product']['id']), array('class'=>'button1'));
            
        ?>
    </p>
</div>