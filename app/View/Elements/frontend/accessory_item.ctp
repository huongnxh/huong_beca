<div class="productlist fix_relative">
    <a href="<?php echo $this->Html->url(array('controller'=>'accessories', 'action'=>'view', $item['Accessory']['id']))?>">
        <?php
            if(!$item['Accessory']['is_new'])
                echo $this->Html->image(Configure::read('settings.imageDir') .'218/'. $item['Accessory']['image']);
            else
                echo $this->Html->image(Configure::read('settings.imageDir') .'218/'. $item['Accessory']['image']).$this->Html->image('frontend/icon_new.png', array('class'=>'icon_new'));
            
        ?>
    </a>
    <a href="<?php echo $this->Html->url(array('controller'=>'accessories', 'action'=>'view', $item['Accessory']['id']))?>"><h3><?php echo $item['Accessory']['name']?></h3></a>
    <p align="justify">
    <?php echo $this->Text->truncate(strip_tags($item['Accessory']['description']), 30, array('ending'=>'...'))?>
    </p>
    <p align="right" style="padding-top:10px;">
        <?php 
            if($item['Accessory']['display_price']){
                echo $this->Html->link($item['Accessory']['price'], array('controller'=>'accessories', 'action'=>'view', $item['Accessory']['id']), array('class'=>'button_gia'));
            }
			echo '&nbsp;';
			echo $this->Html->link('Xem chi tiết', array('controller'=>'accessories', 'action'=>'view', $item['Accessory']['id']), array('class'=>'button1'));
            
        ?>
    </p>
</div>
