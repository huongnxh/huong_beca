<!--paging-->
<div class="paging" align="center">
    <?php
        echo $this->Paginator->prev(__('Trước'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('Sau'), array(), null, array('class' => 'next disabled'));
    ?>
</div>
<!--paging-->