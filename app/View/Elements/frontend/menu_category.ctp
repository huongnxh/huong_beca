<div class="sub_menu">
    <ul>
        <?php
            foreach ($menus as $menu){
				$class = '';
                if(!$menu['Product']){continue;}
				if($menu['Category']['id'] == $category_id){
					$class = 'current';
				}
                echo '<li>'.$this->Html->link($menu['Category']['name'], array('controller'=>'products', 'action'=>'index', $menu['Category']['id']), array('class'=>$class)).'</li>';
            }
        ?>
    </ul>
    <div class="cl"></div>
    <div class="line_orange"></div>
    <ul>
        <?php
            foreach ($menus as $menu){
				$class = '';
                if(!$menu['Accessory']){continue;}
				if($menu['Category']['id'] == $active_id){
					$class = 'current';
				}
                echo '<li>'.$this->Html->link($menu['Category']['name'], array('controller'=>'accessories', 'action'=>'index', $menu['Category']['id']), array('class'=>$class)).'</li>';
            }
        ?>
    </ul>
    <div class="cl"></div>
</div>

