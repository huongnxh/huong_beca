<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("Contact") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>
    <div class="height10"></div>
    <?php echo $this->Form->create('Contact',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('name',__('Name')); ?></th>
            <th><?php echo $this->Paginator->sort('email',__('Email')); ?></th>
            <th><?php echo $this->Paginator->sort('phone',__('Phone')); ?></th>
            <th><?php echo $this->Paginator->sort('created',__('Created')); ?></th>
            <th class="actions" width="15%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($contacts) && count($contacts) > 0): ?>
            <?php foreach ($contacts as $contact): ?>
                <tr>
                    <td><center><?php echo h($contact['Contact']['name']); ?>&nbsp;</center></td>  
                    <td><center><?php echo h($contact['Contact']['email']); ?>&nbsp;</center></td>  
                    <td><center><?php echo h($contact['Contact']['phone']); ?>&nbsp;</center></td>  
                    <td><center><?php echo h($contact['Contact']['created']); ?>&nbsp;</center></td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $contact['Contact']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $contact['Contact']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Contact']['count'] > $this->params['paging']['Contact']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>