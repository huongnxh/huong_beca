<section>
    <!-- bottom-->
    <div class="bottom">
        <!--left-->
        <?php
            echo $this->element('frontend/sidebar_left_gallery');    
        ?>
        <!--left-->
        <!--right-->
        <div class="right">
            <!--tin tuc-->
            <div class="tintuc_blog lienhe">	
                <?php echo $this->Session->flash();?>
                <h1>Bể cá Thái Hà</h1>
                <p>Bạn hãy liên hệ với chúng tôi bằng cách nhập đầy đủ thông tin trong ô dưới đây.<br>
                Xin chân thành cảm ơn</p>
                <?php echo $this->Form->create('Contact', array('action'=>'add'), array('enctype'=>'multipart/form-data'))?>
                <table width="100%">
                    <tr>
                            <th>Họ tên</th>
                        <td><?php echo $this->Form->input('name', array('label'=>false, 'required'=>false))?></td>
                    </tr>
                    <tr>
                            <th>Email</th>
                            <td><?php echo $this->Form->input('email', array('label'=>false, 'required'=>false))?></td>
                    </tr>
                    <tr>
                            <th>Số ĐT</th>
                        <td><?php echo $this->Form->input('phone', array('label'=>false, 'required'=>false))?></td>
                    </tr>
                    <tr>
                            <th>Địa chỉ</th>
                        <td><?php echo $this->Form->input('address', array('label'=>false))?></td>
                    </tr>
                    <tr>
                            <th>Yêu Cầu</th>
                        <td> <?php echo $this->Form->input('content', array('type'=>'textarea', 'label'=>false, 'required'=>false))?></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><?php echo $this->Html->link('Hủy', array('action'=>'add'), array('class'=>'button2'))?> <input type="submit" class="button2" value="Gửi"></td>
                    </tr>
                </table>
                <?php echo $this->Form->end()?>
            </div>
            <!--tin tuc-->
        </div>
        <!--right-->
        <div class="cl"></div>
    </div>
    <!-- bottom-->
</section>
<!-- section end-->
