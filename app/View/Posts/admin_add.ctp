<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo $this->Html->link(__('Posts',true),array('action'=>'index')); ?></span> >     
    <span><?php echo __('PostAdd') ?></span>
</div>

<div id="center">
    
    <div id="right">
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('Post',array('enctype'=>'multipart/form-data')); ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Title') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('title',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Image') ?></th>
                <td width="70%">
                    <?php
                    if(isset($this->request->data['Post']['id'])){
                        echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$this->request->data['Post']['image']);
                        echo '<br>';
                    }
                    echo $this->Form->input('image',array('type'=>'file','size'=>50,'label'=>false,'div'=>false)); 

                    ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('Category') ?><?php echo __('(*)') ?></th>
                <td><?php echo $this->Form->input('post_category_id',array('options'=>Configure::read('settings.post_category.type'),'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Content') ?><?php echo __('(*)') ?></th>
                <td><?php echo $this->Form->input('content',array('class'=>'ckeditor','size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Active') ?></th>
                <td><?php echo $this->Form->input('active',array('label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/posts/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>