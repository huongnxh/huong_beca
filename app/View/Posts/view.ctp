<section>
    <div class="bottom">
        <?php
            echo $this->element('frontend/sidebar_left_gallery');    
        ?>
        <!--right-->
        <div class="right">
            <!--tin tuc-->
            <div class="tintuc_blog">
                <?php if(isset($post) && count($post)>0):?>
                    <div class="green_title"><?php echo $post['Post']['title']?></div><br>
                    <?php echo $post['Post']['content']?>
                <?php endif;?>
                <div class="line_green"></div>
                <div class="recent">
                        <p class="green_text_upper">Tin liên quan</p>
                        <ul>
                            <?php
                                if(isset($recent) && count($recent)>0){
                                    foreach ($recent as $item){
                                        echo '<li>'.$this->Html->link($item['Post']['title'], array('controller'=>'posts', 'action'=>'view', $item['Post']['id'])).' - ('.$this->Time->format($item['Post']['created'], '%B %e, %Y').')'.'</li>';
                                    }
                                }else{
                                    echo 'Chưa có dữ liệu';
                                }    
                            ?>
                        </ul>
                </div>
            </div>
            <!--tin tuc-->
        </div>
    <!--right-->
    <div class="cl"></div>
    </div>
</section>

