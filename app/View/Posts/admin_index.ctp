<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("PostList") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <span class="n_ok" style="display: inline-block">Kéo thả để sắp xếp dữ liệu</span>
        <input type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/posts/add'); ?>';" value="<?php echo __("NewPost") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Post',array('action'=>'sort')); ?>
    <table class="tblList" width="100%">
        <tr class="sort-disabled">
            <th><?php echo $this->Paginator->sort('title',__('Title')); ?></th>
            <th><?php echo $this->Paginator->sort('image',__('Image')); ?></th>
            <th><?php echo $this->Paginator->sort('content',__('Content')); ?></th>
            <th><?php echo $this->Paginator->sort('post_category_id',__('Category')); ?></th>
            <th><?php echo $this->Paginator->sort('active',__('Active')); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php $type = Configure::read('settings.post_category.type')?>
        <?php if(isset($posts) && count($posts) > 0): ?>
            <?php foreach ($posts as $post): ?>
                <tr>
                    <td>
                        <input type="hidden" name = "data[Order][]" value="<?php echo $post['Post']['id'] ?>" />
                        <?php echo h($post['Post']['title']); ?>&nbsp;
                    </td>
                    <td><center><?php echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$post['Post']['image'], array('plugin'=>false)); ?>&nbsp;</center></td> 
                    <td><?php echo $this->Text->truncate(strip_tags($post['Post']['content']), 200, array('ellipsis'=> '...', 'exact'=>false)); ?>&nbsp;</td>
                    <td><?php echo $type[$post['Post']['post_category_id']]; ?>&nbsp;</td>
                    <td>
                        <center>
                        <?php
                            if($post['Post']['active'])
                                echo $this->Html->link($this->Html->image("system/active.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$post['Post']['id'],0),array('escape'=>false));
                            else
                                echo $this->Html->link($this->Html->image("system/lock.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$post['Post']['id'],1),array('escape'=>false));

                        ?>
                        </center>
                    </td>      
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $post['Post']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $post['Post']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $post['Post']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('Không có dữ liệu') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <input id = "btnSubmitSort" type="submit" class="btn_small_blue" value="Sort" style="display:none;">
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Post']['count'] > $this->params['paging']['Post']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>
<script>
$(function() 
{
    $(".tblList tbody").sortable({items: ">*:not(.sort-disabled)",stop: function(event, ui)
    { 
        $("#btnSubmitSort").css('display','inline');
        $(".sort-disabled").hide();
        $(".n_ok").css('display', 'none')
    }});
    $(".tblList tbody").sortable({items: ">*:not(.sort-disabled)"}).disableSelection();
})  ;
</script>