<section>
    <!--left-->
    <?php echo $this->element('frontend/sidebar_left_gallery')?>
    <!--left-->
    <!--right-->
    <div class="right">
    <!--tin tuc-->
    <div class="tintuc_blog">
        <!-- list tin tuc-->
        <?php 
            if(isset($news) && count($news)>0){
                foreach ($news as $new){
                    echo '<div class="list_tintuc2">';
                        echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'200/'.$new['Post']['image'], array('class'=>'thumbnails_tintuc fr')), array('controller'=>'posts', 'action'=>'view', $new['Post']['id']), array('escape'=>false));
                        echo '<div>';
                            echo '<p class="green_title">';
                                echo $this->Html->link($new['Post']['title'], array('controller'=>'posts', 'action'=>'view', $new['Post']['id']));
                            echo '</p>';
                            echo '<p align="justify">';
                                echo $this->Text->truncate(strip_tags($new['Post']['content']), 200, array('ending'=>'....', 'exact'=>true));
                            echo '</p>';
                            echo '<br>';
                            echo '<p align="right">'.$this->Html->link('Xem', array('controller'=>'posts', 'action'=>'view', $new['Post']['id']), array('class'=>'button2')).'</p>';
                        echo '</div>';
                        echo '<div class="cl"></div>';
                    echo '</div>';
                    
                }
            }else {
                echo 'Chưa có dữ liệu';
            }
        ?>
        <!--paging-->
            <?php 
                if($this->params['paging']['Post']['count'] > $this->params['paging']['Post']['limit'])
                    echo $this->element('frontend/paginate');
            ?>
        <!--paging-->
    </div>
    <!--tin tuc-->
    </div>
    <!--right-->
    <div class="cl"></div>
</section>    
