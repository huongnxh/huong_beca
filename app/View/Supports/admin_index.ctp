<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("Supports") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/supports/add'); ?>';" value="<?php echo __("New Support") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Support',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('nick_name'); ?></th>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('phone'); ?></th>
            <th><?php echo $this->Paginator->sort('active'); ?></th>
            <th><?php echo $this->Paginator->sort('type'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($supports) && count($supports) > 0): ?>
            <?php foreach ($supports as $support): ?>
            <?php $type = Configure::read('settings.support.type')?>
                <tr>
                    <td><?php echo h($support['Support']['nick_name']); ?>&nbsp;</td>
                    <td><?php echo h($support['Support']['name']); ?>&nbsp;</td>
                    <td><?php echo h($support['Support']['phone']); ?>&nbsp;</td>
                    <td>
                        <center>
                            <?php 
                                if($support['Support']['active'])
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $support['Support']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $support['Support']['id'], 1), array('escape'=>false));
                            ?>
                        </center>
                    </td>
                    <td><center><?php echo $type[$support['Support']['type']]; ?>&nbsp;</center></td>
                    <td><?php echo h($support['Support']['created']); ?>&nbsp;</td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $support['Support']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $support['Support']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $support['Support']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Support']['count'] > $this->params['paging']['Support']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>
