<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("UserList") ?></span>
</div>

<div id="center">
    <?php echo $this->element('admin/left') ?>
    
    <div id="right_notfull">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/add'); ?>';" value="<?php echo __("NewUser") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('User',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('username',__('Username')); ?></th>
			<th><?php echo $this->Paginator->sort('name',__('Name')); ?></th>
            <th><?php echo $this->Paginator->sort('active'); ?></th>
            <th><?php echo $this->Paginator->sort('group',__('Group')); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($users) && count($users) > 0): ?>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
					<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                    <td>
                        <center>
                        <?php
                            if($user['User']['id'] != 1)
                            {
                                if($user['User']['active'])
                                    echo $this->Html->link($this->Html->image("system/active.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$user['User']['id'],0),array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png",array('width' => 12, 'height' => 12)),array('action' => 'set_active',$user['User']['id'],1),array('escape'=>false));
                            }
                        ?>
                        </center>
                    </td>
                        <td>
                            <?php if(isset($groups[$user['User']['group']])) echo $groups[$user['User']['group']]; else echo '<font color = "#BA0000">'.__('UnknownGroup',true).'</font>'; ?>
                        </td>        
                        <td class="actions">
                            <center>
                            <?php if($user['User']['id'] != 1) echo $this->Html->link($this->Html->image('system/reset.png',array('width'=>14,'height'=>14,'title'=>__('ResetPassword',true),'tooltip'=>__('ResetPassword',true))), array('action' => 'reset_password', $user['User']['id']),array('escape'=>false)); ?>
                            <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'edit', $user['User']['id']),array('escape'=>false)); ?>
                            <?php if($user['User']['id'] != 1) echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $user['User']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $user['User']['id']));?>
                            </center>
                        </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['User']['count'] > $this->params['paging']['User']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>