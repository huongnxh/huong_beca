<div class="breadscrumb">
    <span><?php echo __('Account') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __('UserChangePassword') ?></span>
</div>
<div id="center">
    <?php echo $this->element('admin/left') ?>
    
    <div id="right_notfull">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('User'); ?>
            <?php echo $this->Form->input('id'); ?>
            
            <table width="100%" class="tblForm">
                <tr>
                    <th width="30%"><?php echo __('OldPassword') ?></th>
                    <td width="70%"><?php echo $this->Form->input('old_password',array('type'=>'password','size'=>50,'label'=>false,'div'=>false, 'required'=>false)) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('NewPassword') ?></th>
                    <td><?php echo $this->Form->input('new_password',array('type'=>'password','size'=>50,'label'=>false,'div'=>false, 'required'=>false)) ?></td>
                </tr>
                <tr>
                    <th><?php echo __('ConfirmPassword') ?></th>
                    <td><?php echo $this->Form->input('confirm_password',array('type'=>'password','size'=>50,'label'=>false,'div'=>false, 'required'=>false)) ?></td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button type="submit" class="btn_small_blue"><?php echo __('Change') ?></button>
                        <?php if($curUser['group'] == 1): ?>
                            <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/index'); ?>';"><?php echo __("Back") ?></button>
                        <?php else: ?>
                            <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/dashboard'); ?>';"><?php echo __("Back") ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        <?php echo $this->Form->end(); ?>
    </div>
        
    <div class="cl"></div>
    <div class="height10"></div>
</div>