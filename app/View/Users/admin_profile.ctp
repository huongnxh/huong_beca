<div class="breadscrumb">
    <span><?php echo __('Account') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __('Profile') ?></span>
</div>

<div id="center">
    <?php echo $this->element('admin/left') ?>
    
    <div id="right_notfull">
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
        <?php echo $this->Form->input('id') ?>
        <input type="hidden" name="data[User][username]" value="<?php echo $this->request->data['User']['username'] ?>" />
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Username') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('username',array('size'=>50,'label'=>false,'div'=>false,'disabled'=>'disabled')); ?></td>
            </tr>
			 <tr>
                <th width="30%"><?php echo __('Name') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('name',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <?php if($curUser['group'] == 1): ?>
                        <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/index'); ?>';"><?php echo __("Back") ?></button>
                    <?php else: ?>
                        <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/dashboard'); ?>';"><?php echo __("Back") ?></button>
                    <?php endif; ?>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?> 
    </div> 
    <div class="cl"></div>
    <div class="height10"></div>
</div>

