<div class="breadscrumb">
    <span><?php echo __('Account') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __('ResetPassword') ?></span>
</div>

<div id="center">
    <?php echo $this->element('admin/left') ?>
    
    <div id="right_notfull">
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
        <?php echo $this->Form->input('id') ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Username') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('username',array('size'=>50,'label'=>false,'div'=>false,'disabled'=>'disabled')); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('NewPassword') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('password',array('size'=>50,'label'=>false,'div'=>false, 'value' => false)); ?></td>
            </tr>
			<tr>
                <th width="30%"><?php echo __('ConfirmPassword') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('confirm_password',array('type'=>'password', 'size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
			<tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?> 
    </div> 
    <div class="cl"></div>
    <div class="height10"></div>
</div>

