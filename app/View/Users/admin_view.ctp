<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <?php if($curUser['group'] == 1): ?>
    <span><?php echo $this->Html->link(__('UserList',true),array('action'=>'index')); ?></span> >     
    <?php endif; ?>
    <span><?php echo __('UserView') ?></span>
</div>

<div id="center">
    <?php echo $this->element('admin/left') ?>
    
    <div id="right_notfull">
    <?php echo $this->Session->flash(); ?>
            
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Username') ?></th>
                <td width="70%"><p><?php echo $user['User']['username'] ?></p></td>
            </tr>
			<tr>
                <th width="30%"><?php echo __('Name') ?></th>
                <td width="70%"><p><?php echo $user['User']['name'] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Email') ?></th>
                <td><p><?php echo $user['User']['email'] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Name') ?></th>
                <td><p><?php echo $user['User']['name'] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Phone') ?></th>
                <td><p><?php echo $user['User']['phone'] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Introduction') ?></th>
                <td><p><?php echo $user['User']['note'] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Group') ?></th>
                <td><p><?php echo $groups[$user['User']['group']] ?></p></td>
            </tr>
            <tr>
                <th><?php echo __('Active') ?></th>
                <td><p><?php echo ($user['User']['active'])?__('Active'):__('Not Active') ?></p></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <?php if($curUser['group'] == SYSTEM_ID): ?>
                        <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/index'); ?>';"><?php echo __("Back") ?></button>
                    <?php else: ?>
                        <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/agronomist'); ?>';"><?php echo __("Back") ?></button>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>