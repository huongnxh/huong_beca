<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <?php if($curUser['group'] == 1): ?>
    <span><?php echo $this->Html->link(__('UserList',true),array('action'=>'index')); ?></span> >     
    <?php endif; ?>
    <span><?php echo __('UserEdit') ?></span>
</div>

<div id="center">
    <!--left start-->
    <?php //echo $this->element('admin/left') ?>
    <!--left end-->
    
    <!--right start-->
    <div>
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
        <?php echo $this->Form->input('id') ?>
        <input type="hidden" name="data[User][username]" value="<?php echo $this->request->data['User']['username'] ?>" />
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Username') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('username',array('size'=>50,'label'=>false,'div'=>false,'disabled'=>'disabled')); ?></td>
            </tr>
			<tr>
                <th width="30%"><?php echo __('Name') ?><?php echo __('*') ?></th>
                <td width="70%"><?php echo $this->Form->input('name',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <?php if($this->request->data['User']['id'] != 1): ?>
            <tr>
                <th><?php echo __('Group') ?></th>
                <td><?php echo $this->Form->input('group',array('label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Active') ?></th>
                <td><?php echo $this->Form->input('active',array('label'=>false,'div'=>false)); ?></td>
            </tr>
            <?php endif; ?>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <?php if($curUser['group'] == 1): ?>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/users/index'); ?>';"><?php echo __("Back") ?></button>
                    <?php endif; ?>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>