<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("Categories") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
        <?php echo $this->Session->flash(); ?>

        <div align="right">
            <span class="n_ok" style="display: inline-block">Kéo thả để sắp xếp dữ liệu</span>
            <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/categories/add'); ?>';" value="<?php echo __("New Category") ?>" />
        </div>    

        <div class="height10"></div>
        <?php echo $this->Form->create('Category',array('action'=>'sort'), array('class' => 'FormOrder')); ?>
        <table class="tblList" width="100%">
            <tr class="sort-disabled">
                <th><?php echo $this->Paginator->sort('name',__('Category')); ?></th>
                <th><?php echo $this->Paginator->sort('type',__('Type')); ?></th>
                <th><?php echo $this->Paginator->sort('image',__('Image')); ?></th>
                <th><?php echo $this->Paginator->sort('display', __('Display'));?></th>
                <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
            </tr>
            <?php if(isset($categories) && count($categories) > 0): ?>
                <?php 
                $type = Configure::read('settings.category.type');
                foreach ($categories as $category): 
                ?>
                    <tr>
                        <td>
                            <input type="hidden" name = "data[Order][]" value="<?php echo $category['Category']['id'] ?>" />
                            <center><?php echo h($category['Category']['name']); ?>&nbsp;</center>
                        </td>
                        <td>
                            <center><?php echo h($type[$category['Category']['type']]); ?>&nbsp;</center>
                        </td>
                        <td><center>
						<?php
							if(!empty($category['Category']['image']))
								echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$category['Category']['image'], array('plugin'=>false)); 
							else
								echo $this->element('admin/image');
						?>&nbsp;</center></td> 
                        <td>
                            <center>
                                <?php 
                                    if($category['Category']['display'])
                                        echo $this->Html->link($this->Html->image("system/active.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display', $category['Category']['id'], 0), array('escape'=>false)); 
                                    else
                                        echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display', $category['Category']['id'], 1), array('escape'=>false)); 
                                ?>&nbsp;
                            </center>
                        </td> 
                        <td class="actions">
                            <center>
                            <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $category['Category']['id']),array('escape'=>false)); ?>
                            <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $category['Category']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $category['Category']['id']));?>
                            </center>
                        </td>
                    </tr>

                <?php endforeach; ?>

            <?php else: ?>
                <tr>
                    <td colspan="6"><?php echo __('NoData') ?></td>
                </tr>
            <?php endif; ?>
            </table>
                <input id = "btnSubmitSort" type="submit" class="btn_small_blue" value="Sort" style="display:none;">

            <?php echo $this->Form->end(); ?>

        <?php if($this->params['paging']['Category']['count'] > $this->params['paging']['Category']['limit']): ?>
        <?php echo $this->element('admin/paginator') ?>
        <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>
<script>
$(function() 
{
    $(".tblList tbody").sortable({items: ">*:not(.sort-disabled)",stop: function(event, ui)
    { 
        $("#btnSubmitSort").css('display','inline');
        $(".sort-disabled").hide();
        $(".n_ok").css('display', 'none')
    }});
    $(".tblList tbody").sortable({items: ">*:not(.sort-disabled)"}).disableSelection();
})  ;
</script>