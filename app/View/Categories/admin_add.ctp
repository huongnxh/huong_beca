<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo $this->Html->link(__('Categories',true),array('action'=>'index')); ?></span> >  
	<span><?php
		if(!$id)
			echo __('CategoriesAdd');
		else
			echo __('CategoriesEdit');
	?></span>
</div>

<div id="center">
    
    <div id="right">
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('Category',array('enctype'=>'multipart/form-data')); ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Name Category') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('name',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Display') ?></th>
                <td width="70%"><?php echo $this->Form->input('display',array('label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Image') ?></th>
                <td width="70%"><?php
                if(isset($this->request->data['Category']['id'])){
                    echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$this->request->data['Category']['image']);
                    echo '<br>';
                }
                echo $this->Form->input('image',array('type'=>'file','size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Type') ?></th>
                <td width="70%"><?php echo $this->Form->input('type',array('options'=>Configure::read('settings.category.type'),'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/categories/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>
