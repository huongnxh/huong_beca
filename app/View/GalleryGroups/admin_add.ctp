<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo $this->Html->link(__('GalleryGroup',true),array('action'=>'index')); ?></span> >     
    <span><?php 
		if(!$id)
			echo __('GalleryGroupAdd');
		else
			echo __('GalleryGroupEdit');
	?></span>
</div>

<div id="center">
    
    <div id="right">
    <?php echo $this->Session->flash(); ?>
    
    <?php echo $this->Form->create('GalleryGroup',array('enctype'=>'multipart/form-data')); ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Name Group') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('name',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/galleryGroups/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>
