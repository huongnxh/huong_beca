<section>
    <!--left-->
    <!--gallery_left-->
<!--    <div class="blog_left gallery_left">
            <a href="#"><img src="images/Gallery_small.png"></a><br>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg"></a>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg" style="padding-right:0px;"></a>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg"></a>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg" style="padding-right:0px;"></a>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg"></a>
        <a href="#"><img class="gallery_thumnails_small" src="images/Thumbnails_gallery.jpg" style="padding-right:0px;"></a>
    </div>-->
    <!--gallery_left-->
    <?php echo $this->element('frontend/sidebar_left')?>
    <!--left-->
    <!--right-->
    <div class="right">
    <!--tin tuc-->
        <div class="tintuc_blog">
            <!-- list_blog_gallery-->
            <?php
                if(isset($galleryGroups) && count($galleryGroups)>0){
                foreach ($galleryGroups as $gallery){
                    if(!$gallery['Gallery']){continue;}
                        echo '<div class="list_blog_gallery">';
                            echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'219/'.$gallery['Gallery'][0]['image']), array('controller'=>'galleries', 'action'=>'index', $gallery['GalleryGroup']['id']),array('escape'=>false));
                            echo '<p>'.$this->Html->link($gallery['GalleryGroup']['name'], array('controller'=>'galleries', 'action'=>'index', $gallery['GalleryGroup']['id'])).'</p>';
                        echo '</div>';
                }
            }    
            ?>
            <!-- list_blog_gallery-->

            <div class="cl"></div>
            <!--paging-->
            <?php 
                if($this->params['paging']['GalleryGroup']['count'] > $this->params['paging']['GalleryGroup']['limit'])
                    echo $this->element('frontend/paginate');
            ?>
            <!--paging-->

        </div>
        <!--tin tuc-->
    </div>
    <!--right-->
    <div class="cl"></div>
</section>