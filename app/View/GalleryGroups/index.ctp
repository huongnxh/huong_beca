<section>
    <!--left-->
    <?php echo $this->element('frontend/sidebar_left_gallery')?>
    <!--left-->
    <!--right-->
    <div class="right">
    <!--tin tuc-->
        <div class="tintuc_blog">
            <!-- list_blog_gallery-->
            <?php
            
                if(isset($galleryGroupsContent) && count($galleryGroupsContent)>0){
                    foreach ($galleryGroupsContent as $gallery){
                        //debug($galleryGroups);die;
                            echo '<div class="list_blog_gallery">';
                                echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'219/'.$gallery['Gallery']['image']), array('controller'=>'galleries', 'action'=>'index', $gallery['GalleryGroup']['id']),array('escape'=>false));
                                echo '<p>'.$this->Html->link($gallery['GalleryGroup']['name'], array('controller'=>'galleries', 'action'=>'index', $gallery['GalleryGroup']['id'])).'</p>';
                            echo '</div>';
                    }
                }
                foreach ($videos as $video){
                    echo '<div class="list_blog_gallery">';
                        #sử dụng iframe
                        //echo $this->Html->link($this->Html->image('frontend/video_bg.jpg', array('width'=>219, 'height'=>146)), $video['Video']['url'].'?autoplay=1', array('escape'=>false, 'title'=>$video['Video']['title'], 'class'=>'various fancybox.iframe'));
                        #sử dụng helper media
                        //echo $this->Html->link($this->Html->image('frontend/video_bg.jpg', array('width'=>219, 'height'=>146)), $video['Video']['url'], array('escape'=>false, 'title'=>$video['Video']['title'], 'class'=>'fancybox-media'));
                        //echo '<iframe width="219" height="146" src="//www.youtube.com/embed/jJZil8aoywE?list=FLX6gcxxl0KjxXHEo8a7kl-Q" frameborder="0" allowfullscreen></iframe>';
                        echo $this->Html->link($this->Html->image('frontend/video_bg.jpg', array('width'=>219, 'height'=>146)), array('controller'=>'videos', 'action'=>'view', $video['Video']['id']), array('escape'=>false, 'title'=>$video['Video']['title']));
                        echo '<p>'.$this->Html->link($this->Text->truncate($video['Video']['title'], 30, array('ending'=>'...')), $video['Video']['url'], array('title'=>$video['Video']['title'],'class'=>'fancybox-media')).'</p>';
                    echo '</div>';
                }
            ?>
            <!-- list_blog_gallery-->

            <div class="cl"></div>
            <!--paging-->
            <?php 
                if($this->params['paging']['Gallery']['count'] > $this->params['paging']['Gallery']['limit'])
                    echo $this->element('frontend/paginate');
            ?>
            <!--paging-->

        </div>
        <!--tin tuc-->
    </div>
    <!--right-->
    <div class="cl"></div>
</section>
<script>
//sử dụng helper
$(document).ready(function() {
    $('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                    media : {}
            }
    });
});
//sử dụng iframe
$(document).ready(function() {
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script>