<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("GalleryGroup") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
        <?php echo $this->Session->flash(); ?>

        <div align="right">
            <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/galleryGroups/add'); ?>';" value="<?php echo __("New Category") ?>" />
        </div>    

        <div class="height10"></div>
        <?php echo $this->Form->create('GalleryGroup',array('action'=>'sort'), array('class' => 'FormOrder')); ?>
        <table class="tblList" width="100%">
            <tr class="sort-disabled">
                <th><?php echo $this->Paginator->sort('name',__('GalleryGroup')); ?></th>
                <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
            </tr>
            <?php if(isset($galleryGroups) && count($galleryGroups) > 0): ?>
                <?php foreach ($galleryGroups as $galleryGroup): ?>
                    <tr>
                        <td>
                            <input type="hidden" name = "data[Order][]" value="<?php echo $galleryGroup['GalleryGroup']['id'] ?>" />
                            <center><?php echo h($galleryGroup['GalleryGroup']['name']); ?>&nbsp;</center>
                        </td> 
                        <td class="actions">
                            <center>
                            <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $galleryGroup['GalleryGroup']['id']),array('escape'=>false)); ?>
                            <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $galleryGroup['GalleryGroup']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $galleryGroup['GalleryGroup']['id']));?>
                            </center>
                        </td>
                    </tr>

                <?php endforeach; ?>

            <?php else: ?>
                <tr>
                    <td colspan="6"><?php echo __('NoData') ?></td>
                </tr>
            <?php endif; ?>
            </table>
                <input id = "btnSubmitSort" type="submit" class="btn_small_blue" value="Sort" style="display:none;">

            <?php echo $this->Form->end(); ?>

        <?php if($this->params['paging']['GalleryGroup']['count'] > $this->params['paging']['GalleryGroup']['limit']): ?>
        <?php echo $this->element('admin/paginator') ?>
        <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>
<script>
