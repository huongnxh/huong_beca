<style type="text/css">
    /*set a border on the images to prevent shifting*/
    //#gallery_01 img{border:2px solid white;}

    /*Change the colour*/
    .active img{border:2px solid #333 !important;}
</style>
<section>
    <!-- sub_menu-->
    <?php echo $this->element('frontend/menu_category')?>
    <!-- sub_menu-->
    <!-- san pham chi tiet-->
        <div class="sanphamchitiet">
            <p class="line_green"></p>
            <div class="fl" id="gallery_01">
                <?php echo $this->Html->link(
                        $this->Html->image(Configure::read('settings.imageDir').'408/'.$accessory['Accessory']['image'], array('class'=>'img_large','id'=>'zoom_03', 'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$accessory['Accessory']['image'])),
                        Configure::read('settings.imageDir').'1280/'.$accessory['Accessory']['image'],
                        array('escape'=>false, 'class'=>'fancybox', 'rel'=>'gallery1')
                    )?><br>
                <?php 
                echo $this->Html->link(
                            $this->Html->image(
                                Configure::read('settings.imageDir').'408/'.$accessory['Accessory']['image'], 
                                array('class'=>'img_small')
                            ),
                            "#",
                            array(
                                'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$accessory['Accessory']['image'], 
                                'data-image'=>'../'.Configure::read('settings.imageDir').'408/'.$accessory['Accessory']['image'],
                                'escape'=>false
                            )
                            
                        );
                if($accessory['Gallery']){
                    foreach ($accessory['Gallery'] as $item){
                        echo $this->Html->link(
                            $this->Html->image(
                                Configure::read('settings.imageDir').'408/'.$item['image'], 
                                array('class'=>'img_small')
                            ),
                            "#",
                            array(
                                'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$item['image'], 
                                'data-image'=>'../'.Configure::read('settings.imageDir').'408/'.$item['image'],
                                'escape'=>false
                            )
                        );
                    }
                };
                ?>
            </div>
            <div class="content">
            	<div class="green_title"><?php echo $accessory['Accessory']['name']?></div><br>
                <?php if($accessory['Accessory']['display_price']):?>
                    <div class="Price_detail" ><label>Giá: <?php echo $this->Number->currency($accessory['Accessory']['price'], '', array('thousands'=>'.', 'places'=>0))?></label></div>
                <?php endif;?>
                <?php echo $accessory['Accessory']['description']?>
            </div>
            <div class="cl"></div>
        </div>
        <!-- san pham chi tiet-->
        
        <div class="green_text_upper">sản phẩm liên quan</div>
    	<!-- SP beca start-->
    	<div class="blog_greenbg">
            <!-- dòng 1-->
            <div>
                <?php
                if(isset($accessories) && count($accessories)>0){
                    foreach ($accessories as $accessory){
                        echo $this->element('frontend/accessory_item',array(
                            'item' => $accessory
                        ));
                    }
                }else {
                    echo 'NoData';
                }
                ?>
                <div class="cl"></div>
            </div>
            <!-- dòng 1-->
        </div>
        
        <!-- SP beca end-->

    <!-- phu kien-->
    
    <!-- phu kien-->
    <!-- bottom-->
    <div class="bottom">
        <!--left-->
        <?php echo $this->element('frontend/sidebar_left')?>
        <!--left-->
        <!--right-->
        <?php echo $this->element('frontend/gallery_news')?>
        <!--right-->
        <div class="cl"></div>
    </div>
    <!-- bottom-->
</section>
<!-- section end-->
<script>
    $("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'}); 
    //pass the images to Fancybox
    $("#zoom_03").bind("click", function(e) {  
      var ez =   $('#zoom_03').data('elevateZoom');	
            $.fancybox(ez.getGalleryList());
      return false;
    });
</script>

