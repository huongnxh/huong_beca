<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("Accessories") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/accessories/add'); ?>';" value="<?php echo __("New Accessory") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Accessory',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('name',__('Accessory Name')); ?></th>
            <th><?php echo $this->Paginator->sort('category_id',__('Category')); ?></th>
            <th><?php echo $this->Paginator->sort('code',__('Code')); ?></th>
            <th><?php echo $this->Paginator->sort('price',__('Price')); ?></th>
            <th><?php echo $this->Paginator->sort('display_price', __('Display_Price')); ?></th>
            <th><?php echo $this->Paginator->sort('image',__('Image')); ?></th>
            <th><?php echo $this->Paginator->sort('description',__('Description')); ?></th>
            <th><?php echo $this->Paginator->sort('active', __('Active')); ?></th>
            <th><?php echo $this->Paginator->sort('is_new', __('isNew')); ?></th>
            <th class="actions" width="10%"><?php echo __('Add Images'); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($accessories) && count($accessories) > 0): ?>
            <?php foreach ($accessories as $accessory): ?>
                <tr>
                    <td><center><?php echo h($accessory['Accessory']['name']); ?>&nbsp;</center></td>
                    <td><center><?php echo h($accessory['Category']['name']); ?>&nbsp;</center></td>
                    <td><center><?php echo h($accessory['Accessory']['code']); ?>&nbsp;</center></td>
                    <td><center><?php echo h($accessory['Accessory']['price']); ?>&nbsp;</center></td> 
                    <td>
                        <center>
                            <?php
                                if($accessory['Accessory']['display_price'])
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display_price', $accessory['Accessory']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display_price', $accessory['Accessory']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td><center><?php echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$accessory['Accessory']['image'], array('plugin'=>false)); ?>&nbsp;</center></td> 
                    <td><center><?php echo $this->Text->truncate($accessory['Accessory']['description'], 100); ?>&nbsp;</center></td>
                    <td>
                        <center>
                            <?php
                                if($accessory['Accessory']['active'])
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_active', $accessory['Accessory']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_active', $accessory['Accessory']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php
                                if($accessory['Accessory']['is_new'])
                                    echo $this->Html->link($this->Html->image("system/open.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_new', $accessory['Accessory']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_new', $accessory['Accessory']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php echo $this->Html->link('Thêm ảnh', array('controller'=>'galleries', 'action'=>'accessory_image', $accessory['Accessory']['id']))?>
                        </center>
                    </td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $accessory['Accessory']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $accessory['Accessory']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $accessory['Accessory']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Accessory']['count'] > $this->params['paging']['Accessory']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>