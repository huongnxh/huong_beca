<section>
    <!-- sub_menu-->
    <?php echo $this->element('frontend/menu_category')?>
    <!-- sub_menu-->
    <!-- SP beca start-->
    <div class="blog_greenbg">
        
        <!-- dòng 1-->
        <div>
            <?php
            if(isset($accessories) && count($accessories)>0){
                foreach ($accessories as $accessory){
                    echo $this->element('frontend/accessory_item',array(
                        'item' => $accessory
                    ));
                }
            }else{
                echo 'Chưa có dữ liệu';
            }
            ?>
            <div class="cl"></div>
        </div>
        <!-- dòng 1-->
        <!--paging-->
        <?php 
            if($this->params['paging']['Accessory']['count'] > $this->params['paging']['Accessory']['limit'])
                echo $this->element('frontend/paginate');
        ?>
        <!--paging-->

    </div>
    <!-- SP beca end-->

    <!-- phu kien-->
<!--    <div class="blog_border">
        <h1> Phụ kiện</h1>
        <div style="padding:10px 9px">
             dòng 1
        <div>
            <?php
                if(isset($accessories) && count($accessories)>0){
                    foreach ($accessories as $accessory){
                        echo $this->element('frontend/accessory_item',array(
                            'item' => $accessory
                        ));
                    }
                }
            ?>
            <div class="cl"></div>
        </div>
         dòng 1
        paging
        <?php 
//            if($this->params['paging']['Accessory']['count'] > $this->params['paging']['Accessory']['limit'])
//                echo $this->element('frontend/paginate');
        ?>
        paging
        </div>

    </div>-->
    <!-- phu kien-->
    <!-- bottom-->
    <div class="bottom">
        <!--left-->
        <?php echo $this->element('frontend/sidebar_left')?>
        <!--left-->
        <!--right-->
        <?php echo $this->element('frontend/gallery_news')?>
        <!--right-->
        <div class="cl"></div>
    </div>
    <!-- bottom-->
</section>
<!-- section end-->

