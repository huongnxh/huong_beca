<script>
    videojs.options.flash.swf = "video-js.swf";
  </script>
<?php echo $this->Html->css('frontend/youtube.css') ?>
<section>
    <?php echo $this->element('frontend/sidebar_left_gallery')?>
    
    <div id="fb-root"></div>
    
    <!--right-->
    <div class="right">
    <!--tin tuc-->
        <div class="tintuc_blog">
            <!-- video slilde-->
			<div class="green_title"><?php echo $video['Video']['title']?></div>
			<video id="my_video_1" src="" class="video-js vjs-default-skin" loop="loop" ytcontrols = 'true' autoplay preload="none" width="680" height="380" data-setup='{ "techOrder": ["youtube"], "src": "<?php echo $video['Video']['url'] ?>" }'>
            </video>
            <div class="cl"></div>
            <!--video slide-->.
            
            <div style="height:20px;"></div>
            <!-- list_blog_gallery-->
                <?php 
                    if(isset($videos) && count($videos)>0){
                        foreach($videos as $video){
                            echo '<div class="list_blog_gallery list_video">';
                                echo $this->Html->link($this->Html->image('frontend/default_video.png', array('width'=>219, 'height'=>146)), array('controller'=>'videos', 'action'=>'view', $video['Video']['id']), array('escape'=>false));
                                echo '<p>'.$this->Html->link($this->Text->truncate($video['Video']['title'], 30, array('ending'=>'...')), array('controller'=>'videos', 'action'=>'view', $video['Video']['id'])).'</p>';
                            echo '</div>';
                        }
                        
                    }
                ?>
            <!-- list_blog_gallery-->
            <div class="cl"></div>
            <!--paging-->
            <?php 
                if($this->params['paging']['Video']['count'] > $this->params['paging']['Video']['limit'])
                    echo $this->element('frontend/paginate');
            ?>
            <!--paging-->
           
        </div>
        <!--tin tuc-->
    </div>
    <!--right-->
    <div class="cl"></div>
</section>
<script src="https://vjs.zencdn.net/4.6.1/video.js"></script>
<?php echo $this->Html->script('youtube');?>
