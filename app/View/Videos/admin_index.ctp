<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php  echo __("Videos");?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/videos/add'); ?>';" value="<?php echo __("New Video") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Video',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('title'); ?></th>
            <th><?php echo $this->Paginator->sort('url'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('active'); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($videos) && count($videos) > 0): ?>
            <?php foreach ($videos as $video): ?>
                <tr>
                    <td><?php echo h($video['Video']['title']); ?>&nbsp;</td>
                    <td><?php echo h($video['Video']['url']); ?>&nbsp;</td>
                    <td><?php echo $this->Text->truncate($video['Video']['description'], 150, array('ellipsis'=>'....', 'exact'=>false)); ?>&nbsp;</td>
                    <td>
                        <center>
                            <?php
                                if($video['Video']['active']){
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $video['Video']['id'], 0), array('escape'=>false));
                                }else{
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $video['Video']['id'], 1), array('escape'=>false));
                                }
                            ?>
                        </center>
                    </td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $video['Video']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $video['Video']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $video['Video']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Video']['count'] > $this->params['paging']['Video']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>

