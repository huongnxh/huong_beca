<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo $this->Html->link(__('Videos',true),array('action'=>'index')); ?></span> >     
    <span><?php 
    if(!$id)
        echo __('Add Video'); 
    else
        echo __('Edit Video');
    ?></span>
</div>

<div id="center">
    
    <div id="right">
    <?php echo $this->Session->flash(); ?>
            
    
    <?php echo $this->Form->create('Video',array('enctype'=>'multipart/form-data')); ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Title') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('title',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Youtube Url') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('url',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Description') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('description',array('class'=>'ckeditor','size'=>50,'label'=>false,'div'=>false, 'required'=>true)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Active') ?></th>
                <td width="70%"><?php echo $this->Form->input('active',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/videos/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>


