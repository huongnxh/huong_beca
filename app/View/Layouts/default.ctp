<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon', 'favicon.png');

		echo $this->Html->css('frontend/Reset.css');
                echo $this->Html->css('frontend/styles.css');
                echo $this->Html->css('frontend/tn3/tn3.css');
                echo $this->Html->css('frontend/jquery.fancybox.css?v=2.1.5');
                echo $this->Html->script('frontend/jquery-1.8.3.min.js');
                echo $this->Html->script('frontend/jquery.elevatezoom.js');
                echo $this->Html->script('frontend/jquery.mousewheel-3.0.6.pack.js');   
                echo $this->Html->script('frontend/jquery.fancybox.js?v=2.1.5');
                echo $this->Html->script('frontend/jquery.fancybox-media.js');
                echo $this->Html->script('frontend/jquery.tn3lite.min.js');
                echo $this->Html->script('frontend/jquery.mb.mediaEmbedder.js');

                

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
<!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->
</head>
<body>
    <div class="wrapper">
        <?php echo $this->element('frontend/header')?>
        <!-- section start-->
        <?php echo $this->fetch('content')?>
        <?php echo $this->element('frontend/back_top')?>
        <!-- section end-->
        <?php echo $this->element('frontend/footer')?>
        <?php echo $this->element('sql_dump'); ?>
    </div>
<!--    <a href='#' style='display:scroll;position:fixed;Top:0px;left:5px;'><img src='http://1.bp.blogspot.com/-ggZV3TobCTQ/UPuyJ4fsg2I/AAAAAAAACjo/g3SiPwd6cUE/s1600/tet1.png'/></a>
    <a href='#' style='display:scroll;position:fixed;Top:0px;right:5px;'><img src='http://1.bp.blogspot.com/-A9IhakAQfck/UPuyJkPRa-I/AAAAAAAACjk/LWo2xFK8LF8/s1600/tet2.png'/></a>-->
    <div class="banner-left"></div>
    <div class="banner-right"></div>
    <script>
        $(window).load(function() {
        if (!$('.classHomepage').length) {
            $("html, body").animate({
                scrollTop: 300
            });
        }
    })
    </script>
</body>
</html>
