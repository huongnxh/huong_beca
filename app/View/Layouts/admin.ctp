<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php echo $this->Html->charset(); ?>
<title>
    <?php echo __($title_for_layout); ?>
</title>
<?php
    echo $this->Html->meta('icon');
    echo $this->Html->css('admin/style.css');
    //echo $this->Html->css('jquery-ui/redmond/jquery-ui-1.10.3.custom.css');
    //echo $this->Html->css('jquery-ui/jquery.ui.timepicker.css');
    //echo $this->Html->css('jquery.largephotobox.css');
    
    
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    
    //echo $this->Html->script('jquery-ui-1.10.3.custom.min.js');
    //echo $this->Html->script('jquery.ui.timepicker.js');
    //echo $this->Html->script('ejs.js');
    //echo $this->Html->script('jquery.largephotobox.js');
    //echo $this->Html->script('jquery.numeric.js');
    echo $this->Html->script('ckeditor/ckeditor.js');
    echo $this->Html->script('jquery');
    echo $this->Html->script('jquery-ui');
    echo $this->Html->script('global');
    //echo $this->Html->script('jquery.form.js');
    //echo $this->Html->script('sortCategories.js');
?>
</head>
<body>

<div id="wrapper">
    <?php echo $this->element('admin/header') ?>      
            
    <!--center start-->
    <?php echo $this->fetch('content'); ?>
    <!--center end-->    
    
    <?php echo $this->element('admin/footer') ?>
    <?php //echo $this->element('sql_dump'); ?>
</div>

</body>
</html>