<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php 
		echo __("Galleries") 
	?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/galleries/add'); ?>';" value="<?php echo __("New Support") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Gallery',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('image'); ?></th>
            <th><?php echo $this->Paginator->sort('gallery_group_id'); ?></th>
            <th><?php echo $this->Paginator->sort('caption'); ?></th>
            <th><?php echo $this->Paginator->sort('active'); ?></th>
            <th class="actions" width="20%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($galleries) && count($galleries) > 0): ?>
            <?php foreach ($galleries as $gallery):?>
                <tr>
                    <td><?php echo $this->Html->image(Configure::read('settings.imageDir') .'50/' .$gallery['Gallery']['image'], array('plugin'=>false)); ?>&nbsp;</td>
                    <td><?php echo h($gallery['GalleryGroup']['name']); ?>&nbsp;</td>
                    <td><?php echo h($gallery['Gallery']['caption']); ?>&nbsp;</td>
                    <td>
                        <center>
                            <?php
                                if($gallery['Gallery']['active']){
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $gallery['Gallery']['id'], 0), array('escape'=>false));
                                }else{
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width' => 12, 'height' => 12)), array('action'=>'set_active', $gallery['Gallery']['id'], 1), array('escape'=>false));
                                }
                            ?>
                        </center>
                    </td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $gallery['Gallery']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $gallery['Gallery']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $gallery['Gallery']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Gallery']['count'] > $this->params['paging']['Gallery']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>
