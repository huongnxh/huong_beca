<?php
echo $this->Form->create('Gallery', array('type' => 'file'));
?>
<div id="center">
    <div id="right">
    <div class="height10"></div>
    <table width="100%" class="tblForm">
        <tbody><tr>
            <th width="25%"> Ảnh của sản phẩm: </th>
            <td>
                <?php
                foreach ($productImages as $image) {
                    echo '<span class="deleteImageProduct" data-delete="' . Router::url(array('action' => 'gallery_delete', $image['Gallery']['id']), true) . '">';
                    echo $this->Html->image(Configure::read('settings.imageDir') . '50/' . $image['Gallery']['image'], array('title' => __('Click to delete'), 'class' => 'imageProducts'));
                    echo '</span>';
                }
                ?>
                <br>
                <p style="clear: both;padding-top: 5px"><i><?php echo __('Click image to delete'); ?></i></p>
            </td>
        </tr>
        <tr>
            <th> <?php echo __('Add images') ?> </th>
            <td>
                <?php
                echo '<div class="input">';
                for($i = 0; $i < 2; $i++) {
                    echo '<div class="imageItem">';
                    echo $this->Form->file('Gallery.' . $i .'.image', array('class' => 'fileInput', 'required' => true));
                    echo $this->Form->hidden('Gallery.' . $i . '.accessory_id', array('default' => $product_id));
                    if($i != 0){
						echo '<a href="#" class="btn_small_blue deleteImage">' . __('Delete') . '</a>';
					}
                    echo '<div class="height10"></div></div>';
                }
                echo '</div>';
                echo ' <a data-product-id="' . $product_id . '" type="submit" class="btn_small_blue addImage">' . __('Thêm ảnh') . '</a>';
                ?>

            </td>
        </tr>
        </tbody>
    </table>
   
    <div class="height10"></div>
    <div  align="center">
        <?php
        echo $this->Form->submit(__('Add'), array(
            'class' => 'btn_small_blue AddImage',
            'div' => false
        ));

        ?>
		<button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/products/index'); ?>';"><?php echo __("Back") ?></button>
    </div>
     
    </div>
    <div class="cl"></div>
    <div class="height10"></div>
</div>

<?php
echo $this->Form->end();
?>