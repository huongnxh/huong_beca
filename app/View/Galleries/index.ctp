<section>
    <!--left-->
    <?php echo $this->element('frontend/sidebar_left_gallery')?>
    <!--left-->
    
    <div id="fb-root"></div>
    
    <!--right-->
    <div class="right">
    <!--tin tuc-->
        <div class="tintuc_blog">
            <!-- gallery slilde-->
            <div class="mygallery">
            <div class="tn3 album">
                <ol>
                    <?php
                    if(isset($galleries) && count($galleries)>0){
                        foreach ($galleries as $gallery){
                            echo '<li>';
                            echo '<h4>'.$gallery['GalleryGroup']['name'].'</h4>';
                            echo '<div class="tn3 description">'.$gallery['Gallery']['caption'].'</div>';
                            echo $this->Html->link(
                                    $this->Html->image(Configure::read('settings.imageDir').'35/'.$gallery['Gallery']['image']), 
                                    Configure::read('settings.imageDir').'620/'.$gallery['Gallery']['image'],
                                    array('escape'=>false)
                                );
                            echo '</li>';
                        }
                    }  
                    ?>
                </ol>
            </div>
            </div>
            <div class="cl"></div>
            <!--gallery slide-->.
            
            <div style="height:20px;"></div>
            <!-- list_blog_gallery-->
                <?php 
                    if(isset($galleryGroupsContent) && count($galleryGroupsContent)>0){
                        foreach($galleryGroupsContent as $galleryGroup){
                            echo '<div class="list_blog_gallery">';
                                
                                echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir').'219/'. $galleryGroup['Gallery']['image']), array('controller'=>'galleries', 'action'=>'index', $galleryGroup['GalleryGroup']['id']), array('escape'=>false));
                                echo '<p>'.$galleryGroup['GalleryGroup']['name'].'</p>';
                            echo '</div>';
                        }
                        
                    }
                ?>
            <!-- list_blog_gallery-->
            <div class="cl"></div>
            <!--paging-->
            <?php 
                if($this->params['paging']['Gallery']['count'] > $this->params['paging']['Gallery']['limit'])
                    echo $this->element('frontend/paginate');
            ?>
            <!--paging-->
           
        </div>
        <!--tin tuc-->
    </div>
    <!--right-->
    <div class="cl"></div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        /*
         *  Simple image gallery. Uses default settings
         */
        $('.fancybox').fancybox({
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    });
</script>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
$(document).ready(function() {
    //Thumbnailer.config.shaderOpacity = 1;
    var tn1 = $('.mygallery').tn3({
        skinDir:"skins",
        imageClick:"fullscreen",
        image:{
            maxZoom:1.5,
            crop:true,
            clickEvent:"dblclick",
            transitions:[{
            type:"blinds"
            },{
            type:"grid"
            },{
            type:"grid",
            duration:460,
            easing:"easeInQuad",
            gridX:1,
            gridY:8,
            // flat, diagonal, circle, random
            sort:"random",
            sortReverse:false,
            diagonalStart:"bl",
            // fade, scale
            method:"scale",
            partDuration:360,
            partEasing:"easeOutSine",
            partDirection:"left"
        }]
        }
    });
});         
</script>
