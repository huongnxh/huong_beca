<section>
    <!-- SP beca start-->
    <div class="blog_greenbg classHomepage">
        <!-- dòng 1-->
        <div>
            <?php if(isset($categories) && count($categories)>0){
                    foreach ($categories as $item){
                        ?>
                        <div class="productlist">
                            <?php  
								if($item['Category']['image']){
									echo $this->Html->link($this->Html->image(Configure::read('settings.imageDir') .'218/'. $item['Category']['image']), array('controller'=>'products', 'action'=>'index', $item['Category']['id']), array('escape'=>false));
								}else{
									echo $this->element('image');
								}
							?>
                            <h3><?php echo $this->Html->link($item['Category']['name'], array('controller'=>'products', 'action'=>'index', $item['Category']['id']))?></h3>
                            <p align="justify">
                            </p>
                        </div>

                    <?php
                    }
                }else
                    echo 'No data';
            ?>
        </div>
        <div class="cl"></div>
        <!-- dòng 1-->
    </div>
    <!-- SP beca end-->

    <!-- phu kien-->
	<?php if(isset($accessories) && count($accessories)>0):?>
		<div class="blog_border">
			<h1> Phụ kiện</h1>
			<div style="padding:10px 9px">
				<!-- dòng 1-->
			<div>
				<?php
					if(isset($accessories) && count($accessories)>0){
						foreach ($accessories as $accessory){
				?>
							<div class="productlist">
								<a href="<?php echo $this->Html->url(array('controller'=>'accessories', 'action'=>'index', $accessory['Category']['id']))?>"><?php  echo $this->Html->image(Configure::read('settings.imageDir') .'218/'. $accessory['Category']['image'])?></a>
								<a href="<?php echo $this->Html->url(array('controller'=>'accessories', 'action'=>'index', $accessory['Category']['id']))?>"><h3><?php echo $accessory['Category']['name']?></h3></a>
								<p align="justify">
								</p>
							</div>
				<?php
						}
					}
				?>
				<div class="cl"></div>
			</div>
			<!-- dòng 1-->
			</div>

		</div>
	<?php endif;?>
    <!-- phu kien-->
    <!-- bottom-->
    <div class="bottom">
        <?php
            echo $this->element('frontend/sidebar_left');
            echo $this->element('frontend/gallery_news');    
        ?>
        <div class="cl"></div>
    </div>
    <!-- bottom-->
    
</section>