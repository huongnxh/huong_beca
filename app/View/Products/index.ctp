<section>
    <!-- sub_menu-->
    <?php echo $this->element('frontend/menu_category')?>
    <!-- sub_menu-->
    <!-- SP beca start-->
    <div class="blog_greenbg">
        
        <!-- dòng 1-->
        <div>
            <?php
            if(isset($products) && count($products)>0){
                foreach ($products as $product){
                    echo $this->element('frontend/product_item',array(
                        'item' => $product
                    ));
                }
            }else{
                echo 'Không có sản phẩm nào';
            }
            ?>
            <div class="cl"></div>
        </div>
        <!-- dòng 1-->
        <!--paging-->
        <?php 
            if($this->params['paging']['Product']['count'] > $this->params['paging']['Product']['limit'])
                echo $this->element('frontend/paginate');
        ?>
        <!--paging-->
    </div>
    <!-- SP beca end-->
	
	<!-- phu kien-->
	<?php if($accessories): ?>
    <div class="blog_border">
        <h1> Phụ kiện</h1>
        <div style="padding:10px 9px">
        <div>
            <?php
                if(isset($accessories) && count($accessories)>0){
                    foreach ($accessories as $accessory){
                        echo $this->element('frontend/accessory_item',array(
                            'item' => $accessory
                               
                        ));
                    }
                }
            ?>
            <div class="cl"></div>
        </div>
        </div>
        <!--paging-->
        <?php 
            //if($this->params['paging']['Accessory']['count'] > $this->params['paging']['Accessory']['limit'])
                //echo $this->element('frontend/paginate');
        ?>
        <!--paging-->
    </div>
	<?php endif;?>
    <!-- phu kien-->
    
    <!-- bottom-->
    <div class="bottom">
        <!--left-->
        <?php echo $this->element('frontend/sidebar_left')?>
        <!--left-->
        <!--right-->
        <?php echo $this->element('frontend/gallery_news')?>
        <!--right-->
        <div class="cl"></div>
    </div>
    <!-- bottom-->
</section>
<!-- section end-->

