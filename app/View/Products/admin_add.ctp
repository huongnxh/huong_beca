<div class="breadscrumb">
    <span><?php echo __('System') ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo $this->Html->link(__('Product',true),array('action'=>'index')); ?></span> >     
	<span><?php
		if(!$id)
			echo __('ProductAdd');
		else
			echo __('ProductEdit');
	?></span>
</div>

<div id="center">
    
    <div id="right">
    <?php echo $this->Session->flash(); ?>
            
    
    <?php echo $this->Form->create('Product',array('enctype'=>'multipart/form-data')); ?>
        <table width="100%" class="tblForm">
            <tr>
                <th width="30%"><?php echo __('Name Product') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('name',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Category') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('category_id',array('label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Code') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('code',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Price') ?><?php echo __('(*)') ?></th>
                <td width="70%"><?php echo $this->Form->input('price',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Dislay_Price') ?></th>
                <td width="70%"><?php echo $this->Form->input('display_price',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Description') ?></th>
                <td width="70%"><?php echo $this->Form->input('description',array('class'=>'ckeditor','label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Image') ?></th>
                <td width="70%"><?php 
                    if(isset($this->request->data['Product']['id'])){
                        echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$this->request->data['Product']['image']);
                        echo '<br>';
                    }
                    echo $this->Form->input('image',array('type'=>'file','size'=>50,'label'=>false,'div'=>false)); 
                ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('Active') ?></th>
                <td width="70%"><?php echo $this->Form->input('active',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th width="30%"><?php echo __('isNew') ?></th>
                <td width="70%"><?php echo $this->Form->input('is_new',array('size'=>50,'label'=>false,'div'=>false)); ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <button type="submit" class="btn_small_blue"><?php echo __('Save') ?></button>
                    <button type = "button" class="btn_small_blue" onclick="location.href='<?php echo $this->Html->url('/admin/products/index'); ?>';"><?php echo __("Back") ?></button>
                </td>
            </tr>
        </table>        
    <?php echo $this->Form->end(); ?>
    
    </div>
    <!--right end-->
    
    <div class="cl"></div>
    <div class="height10"></div>
</div>
