<div class="breadscrumb">
    <span><?php echo __("System") ?></span>
    <?php echo $this->Html->image('admin/brk_center.png') ?>
    <span><?php echo __("Products") ?></span>
</div>

<div id="center">
    
    <div id="right">
    
    <?php echo $this->Session->flash(); ?>

    <div align="right">
        <input type = "button" class="btn_medium_blue" onclick="location.href='<?php echo $this->Html->url('/admin/products/add'); ?>';" value="<?php echo __("New Product") ?>" />
    </div>    
    
    <div class="height10"></div>
    <?php echo $this->Form->create('Product',array('action'=>'index')); ?>
    <table class="tblList" width="100%">
        <tr>
            <th><?php echo $this->Paginator->sort('name',__('Product Name')); ?></th>
            <th><?php echo $this->Paginator->sort('category_id',__('Category')); ?></th>
            <th><?php echo $this->Paginator->sort('code',__('Code')); ?></th>
            <th><?php echo $this->Paginator->sort('code',__('Price')); ?></th>
            <th><?php echo $this->Paginator->sort('active', __('Display_Price')); ?></th>
            <th><?php echo $this->Paginator->sort('image',__('Image')); ?></th>
            <th><?php echo $this->Paginator->sort('description',__('Description')); ?></th>
            <th><?php echo $this->Paginator->sort('active', __('Active')); ?></th>
            <th><?php echo $this->Paginator->sort('is_new', __('isNew')); ?></th>
            <th class="actions" width="10%"><?php echo __('Add Images'); ?></th>
            <th class="actions" width="15%"><?php echo __('Actions'); ?></th>
        </tr>
        <?php if(isset($products) && count($products) > 0): ?>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><center><?php echo h($product['Product']['name']); ?>&nbsp;</center></td>
                    <td><center><?php echo h($product['Category']['name']); ?>&nbsp;</center></td> 
                    <td><center><?php echo h($product['Product']['code']); ?>&nbsp;</center></td>
                    <td><center><?php echo h($product['Product']['price']); ?>&nbsp;</center></td> 
                    <td>
                        <center>
                            <?php
                                if($product['Product']['display_price'])
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display_price', $product['Product']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_display_price', $product['Product']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td><center><?php echo $this->Html->image(Configure::read('settings.imageDir').'50/'.$product['Product']['image'], array('plugin'=>false)); ?>&nbsp;</center></td> 
                    <td><center><?php echo $this->Text->truncate(($product['Product']['description']), 30, array('ending'=>'...')); ?>&nbsp;</center></td>
                    <td>
                        <center>
                            <?php
                                if($product['Product']['active'])
                                    echo $this->Html->link($this->Html->image("system/active.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_active', $product['Product']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_active', $product['Product']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php
                                if($product['Product']['is_new'])
                                    echo $this->Html->link($this->Html->image("system/open.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_new', $product['Product']['id'], 0), array('escape'=>false));
                                else
                                    echo $this->Html->link($this->Html->image("system/lock.png", array('width'=>12, 'hight'=>12)), array('action'=>'set_new', $product['Product']['id'], 1), array('escape'=>false));
                            ?>&nbsp;
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php echo $this->Html->link('Thêm ảnh', array('controller'=>'galleries', 'action'=>'product_image', $product['Product']['id']))?>
                        </center>
                    </td>
                    <td class="actions">
                        <center>
                        <?php echo $this->Html->link($this->Html->image('system/edit.png',array('width'=>14,'height'=>14,'title'=>__('Edit',true),'tooltip'=>__('Edit',true))), array('action' => 'add', $product['Product']['id']),array('escape'=>false)); ?>
                        <?php echo $this->Html->link($this->Html->image('system/delete.png',array('width'=>14,'height'=>14,'title'=>__('Delete',true),'tooltip'=>__('Delete',true))), array('action' => 'delete', $product['Product']['id']), array('escape'=>false), sprintf(__('MsgConfirmDeleteUser', true), $product['Product']['id']));?>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6"><?php echo __('NoData') ?></td>
            </tr>
            <?php endif; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        
    <?php if($this->params['paging']['Product']['count'] > $this->params['paging']['Product']['limit']): ?>
    <?php echo $this->element('admin/paginator') ?>
    <?php endif; ?>
    
    </div>
    <!--right end-->
    <div class="cl"></div>
    <div class="height10"></div>
</div>