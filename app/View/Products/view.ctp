<style type="text/css">
    /*set a border on the images to prevent shifting*/
    //#gallery_01 img{border:2px solid white;}

    /*Change the colour*/
    .active img{border:2px solid #333 !important;}
</style>
<section>
    <!-- sub_menu-->
    <?php echo $this->element('frontend/menu_category')?>
    <!-- sub_menu-->
    <!-- san pham chi tiet-->
        <div class="sanphamchitiet">
            <p class="line_green"></p>
            <div class="fl" id="gallery_01">
                <?php echo $this->Html->link(
                        $this->Html->image(Configure::read('settings.imageDir').'408/'.$product['Product']['image'], array('class'=>'img_large','id'=>'zoom_03', 'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$product['Product']['image'])),
                        Configure::read('settings.imageDir').'1280/'.$product['Product']['image'],
                        array('escape'=>false, 'class'=>'fancybox', 'rel'=>'gallery1')
                    )
                ?><br>
                <?php echo $this->Html->link(
                            $this->Html->image(
                                Configure::read('settings.imageDir').'408/'.$product['Product']['image'], 
                                array('class'=>'img_small')
                            ),
                            "#",
                            array(
                                'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$product['Product']['image'], 
                                'data-image'=>'../'.Configure::read('settings.imageDir').'408/'.$product['Product']['image'],
                                'escape'=>false
                            )
                            
                        );
                        
                        if($product['Gallery']){
							$index = 0;
                            foreach ($product['Gallery'] as $item){
								if($index >=3) {
                                    break;
                                }
                                echo $this->Html->link(
                                    $this->Html->image(
                                        Configure::read('settings.imageDir').'408/'.$item['image'], 
                                        array('class'=>'img_small')
                                    ),
                                    "#",
                                    array(
                                        'data-zoom-image'=>'../'.Configure::read('settings.imageDir').'1280/'.$item['image'], 
                                        'data-image'=>'../'.Configure::read('settings.imageDir').'408/'.$item['image'],
                                        'escape'=>false
                                    )
                                );
								$index ++;
                            }
                        };
                ?>
            </div>
            <div class="content">
                <div class="green_title"><?php echo $product['Product']['name']?></div><br>
                <?php if($product['Product']['display_price']):?>
                    <div class="Price_detail" ><label>Giá: <?php echo $product['Product']['price']?></label></div>
                <?php endif;?>
                <?php echo $product['Product']['description']?>
            </div>
            <div class="cl"></div>
        </div> 
        <!-- san pham chi tiet-->
        
        <div class="green_text_upper">sản phẩm liên quan</div>
    	<!-- SP beca start-->
    	<div class="blog_greenbg">
            <!-- dòng 1-->
            <div>
                <?php
                if(isset($products) && count($products)>0){
                    foreach ($products as $product){
                        echo $this->element('frontend/product_item',array(
                            'item' => $product
                        ));
                    }
                }
                ?>
                <div class="cl"></div>
            </div>
            <!-- dòng 1-->
            
        </div>
        <!-- SP beca end-->

    <!-- phu kien-->
    
    <!-- phu kien-->
    <!-- bottom-->
    <div class="bottom">
        <!--left-->
        <?php echo $this->element('frontend/sidebar_left')?>
        <!--left-->
        <!--right-->
        <?php echo $this->element('frontend/gallery_news')?>
        <!--right-->
        <div class="cl"></div>
    </div>
    <!-- bottom-->
</section>
<!-- section end-->
<script>
    $("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'});   
    //pass the images to Fancybox
    $("#zoom_03").bind("click", function(e) {  
      var ez =   $('#zoom_03').data('elevateZoom');	
            $.fancybox(ez.getGalleryList());
      return false;
    });
</script>

