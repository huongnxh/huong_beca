<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
        Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
		
		Router::connect('/trang-chu', array('controller' => 'pages', 'action' => 'home'));
        Router::connect('/gioi-thieu', array('controller' => 'posts', 'action' => 'about'));
        Router::connect('/san-pham', array('controller' => 'products', 'action' => 'index'));
        Router::connect('/dich-vu', array('controller' => 'posts', 'action' => 'services'));
        Router::connect('/kien-thuc-be-ca', array('controller' => 'posts', 'action' => 'kien_thuc_be_ca'));
        Router::connect('/tin-tuc', array('controller' => 'posts', 'action' => 'news'));
        Router::connect('/lien-he', array('controller' => 'contacts', 'action' => 'add'));
        
        Router::connect('/bai-viet/*', array('controller' => 'posts', 'action' => 'view'), array(
            'pass' => array('id'),
            'id' => '[0-9]+'
            ));
        Router::connect('/danh-muc-san-pham/*', array('controller' => 'products', 'action' => 'index'), array(
            'pass' => array('id'),
            'id' => '[0-9]+'
            ));
        Router::connect('/phu-kien/*', array('controller' => 'accessories', 'action' => 'index'), array(
            'pass' => array('id'),
            'id' => '[0-9]+'
            ));
        
        Router::connect('/san-pham/chi-tiet/*', array('controller' => 'products', 'action' => 'view'), array(
            'pass' => array('id'),
            'id' => '[0-9]+'
            ));
//        Router::connect('/phu-kien-chi-tiet/*', array('controller' => 'accessories', 'action' => 'view'), array(
//            'pass' => array('id'),
//            'id' => '[0-9]+'
//            ));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
