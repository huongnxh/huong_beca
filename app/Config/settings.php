<?php

$config['settings'] = array(
    'post_category' => array(
        'type' => array(
            GIOI_THIEU => __('Giới thiệu', true),
            DICH_VU => __('Dịch vụ', true),
            KIEN_THUC_BE_CA => __('Kiến thức bể cá', true),
            TIN_TUC => __('Tin tức', true),
        )
    ),
    'support' => array(
      'type' => array(
            1 => __('Skype'),
            2 => __('Yahoo'),
            3 => __('Hotline')
      )
    ),
    'category'=>array(
        'type'=>array(
            SAN_PHAM => __('Sản phẩm'),
            PHU_KIEN => __('Phụ kiện'),
        )
    ),
    'uploadDir' => WWW_ROOT . '/files/uploads/',
    'imageDir' => '../files/uploads/',
	'user' => array(
        'character' => array(
            '!', '@', '#', '$', '%', '^', '&', '*', '~', '>', '<', '?', '{', '}', '(', ')'
        )
    ),
);
$config['Config'] = array(
    'language' => 'vie',
);

