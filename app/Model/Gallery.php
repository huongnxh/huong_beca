<?php
App::uses('AppModel', 'Model');
/**
 * Gallery Model
 *
 * @property GalleryGroup $GalleryGroup
 */
class Gallery extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	//Validate
	public $validate = array(
		'caption' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Bạn chưa nhập trường này'
			)
		),
	);
	
/**
 * belongsTo associations
 *
 * @var array
 */ 
    public $belongsTo = array(
		'GalleryGroup' => array(
			'className' => 'GalleryGroup',
			'foreignKey' => 'gallery_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Accessory' => array(
			'className' => 'Accessory',
			'foreignKey' => 'accessory_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
                
	);
        public $actsAs = array(
		'Uploader.FileValidation' => array(
			'image' => array(
				'extension' => array('gif', 'jpg', 'png', 'jpeg'),
				'type' => 'image',
				'required' => true,
			)
		),
        'Uploader.Attachment' => array(
            'image' => array(
                'tempDir' => TMP,
				'nameCallback' => 'renameUploadFile',
                'extension' => array('gif', 'jpg', 'png', 'jpeg', 'swf'),
                'type' => 'image',
                'finalPath' => '',
                'mimeType' => array('image/gif'),
                'quality' => 100,
                'transforms' => array(
                    'thumb_35' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 35,
                        'height' => 35,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_50' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 50,
                        'height' => 50,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_114' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 114,
                        'height' => 72,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_118' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 118,
                        'height' => 82,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_219' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 219,
                        'height' => 146,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_100' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 100,
                        'height' => 72,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_408' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 408,
                        'height' => 308,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_1280' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 1280,
                        'height' => 854,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_620' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 620,
                        'height' => 378,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_926' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 926,
                        'height' => 360,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_960' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 960,
                        'height' => 369,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    )
                )
            )
        )
    );
}

