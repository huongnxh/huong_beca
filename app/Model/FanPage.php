<?php
App::uses('AppModel', 'Model');
/**
 * FanPage Model
 *
 */
class FanPage extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fan_pages';
	public $validate = array(
		'url' => array(
			'rule' => array('notEmpty'),
			'message' => 'Bạn chưa nhập trường này'
		)
	);
}
