<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
	public function getListGroup()
	{
		$group = array
		(
			1 => 'Admin',
			2 => 'Member'
		);
		return $group;
	}
	
	public $validate = array(
            'username' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Tên đăng nhập không được để trống!'
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message' => 'Tên đăng nhập đã tồn tại'
                ),
                'special' => array(
                    'rule'    => '/^[a-z0-9]{3,}$/i',
                    'message' => 'Không được nhập ký tự đặc biệt và phải nhập từ 3 ký tự trở lên'
                )
            ),
			'name' => array(
                'notEmpty' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Tên người dùng không được để trống!'
                ),
            ),
            'password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Mật khẩu không được trống',
                ),
                'minLength' => array(
                    'rule'    => array('minLength', 6),
                    'message' => 'Mật khẩu phải nhập từ 6 ký tự trở lên'
                ),
                'maxLength' => array(
                    'rule'    => array('maxLength', 15),
                    'message' => 'Mật khẩu quá dài'
                ),

            ),
            'new_password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Mật khẩu không được trống',
                ),
                'minLength' => array(
                    'rule'    => array('minLength', 6),
                    'message' => 'Mật khẩu phải từ 6 ký tự'
                ),
                'maxLength' => array(
                    'rule'    => array('maxLength', 15),
                    'message' => 'Mật khẩu quá dài'
                ),
            ),
            'old_password'  => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Mật khẩu không được trống'
                ),
                'checkPass' => array(
                    'rule' =>'checkOldPassword',
                    'message' => 'Mật khẩu cũ không đúng'
                )
            ),
            'confirm_password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Mật khẩu không được trống'
                ),
                'confirmPass' => array(
                    'rule' => 'checkConfirmPassword',
                    'message' => 'Mật khẩu không trùng nhau'
                )
            ),
	);
	/**
     * check special character
     * @return bool
     */
    public function checkSpecialCharacter() {
        $userName = $this->request->data['User']['username'];
        $specialCharacters = Configure::read('settings.user.character');
        foreach ($specialCharacters as $character) {
            if (strpos($userName, $character)) {
                return false;
                break;
            }
        }
        return true;
    }
    
    /**
     * validate old password
     * @return bool
     */
    public function checkOldPassword()
    {
        $check = false;

        $oldPassword = $this->data['User']['old_password'];
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $this->curUser['id'],
                'User.password' => Security::hash($oldPassword, 'sha1', true)
            )
        ));
        if ($user) {
            $check = true;
        }
        return $check;
    }

    /**
     * validate confirm password
     * @return bool
     */
    public function checkConfirmPassword()
    {
        if(isset($this->data['User']['password'])){
            $this->data['User']['new_password'] = $this->data['User']['password'];
        }
        return ($this->data['User']['new_password'] === $this->data['User']['confirm_password']);
    }
    
    /**
     * before save
     *  - encrypt password for user
     * @param array $options
     * @return bool
     */
    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if (isset($this->data[$this->alias]['new_password'])) {
            $this->data[$this->alias]['password'] = $this->data[$this->alias]['new_password'];
        }
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'], 'sha1', true);
        }
        return true;
    }
}
