<?php
App::uses('AppModel', 'Model');
/**
 * Support Model
 *
 */
class Support extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $validate = array(
		'nick_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Bạn chưa nhập trường này'
			),
			'isUnique' => array(
				'rule' => array('checkIsUnique'),
				'message' => 'Nick Name đã tồn tại'
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Bạn chưa nhập trường này'
			),
		),
		'phone' => array(
			'rule' => array('numeric'),
			'message' => 'Điện thoại chỉ được nhập số'
		)
	);
	public function checkIsUnique() {
		$result = $this->find('first', array(
			'conditions' => array(
				'AND' => array(
					'Support.nick_name' => $this->data['Support']['nick_name'],
					'Support.type' => $this->data['Support']['type'],
				)
			)
		));
		if(!$result){
			return true;
		}
		return false;
	}
}
