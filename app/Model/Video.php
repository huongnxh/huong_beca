<?php
App::uses('AppModel', 'Model');
/**
 * Video Model
 *
 */
class Video extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
/*
* Validate
*/
public $validate = array(
	'title' => array(
		'rule' => array('notEmpty'),
		'message' => 'Bạn chưa nhập trường này'
	),
	'description' => array(
		'rule' => array('notEmpty'),
		'message' => 'Bạn chưa nhập trường này'
	),
	'url' => array(
		'notEmpty' => array(
			'rule' => array('notEmpty'),
			'message' => 'Bạn chưa nhập trường này'
		),
		'url' => array(
			'rule' => 'url',
			'message' => 'Bạn nhập chưa đúng url'
		)
	)
);	

}
