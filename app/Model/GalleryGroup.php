<?php
App::uses('AppModel', 'Model');
/**
 * GalleryGroup Model
 *
 * @property Gallery $Gallery
 */
class GalleryGroup extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	//Validate
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			'message' => 'Bạn chưa nhập trường này'
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Tên nhóm đã tồn tại'
			),
		),
	);
	
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Gallery' => array(
			'className' => 'Gallery',
			'foreignKey' => 'gallery_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
