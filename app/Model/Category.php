<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Post $Post
 */
class Category extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        public $validate = array(
            'price' => array(
                'rule' => array('notEmpty'),
                'message' => 'Bạn chưa nhập trường này',
            ),
            'name' => array(
				'isUnique' => array(
					'rule' => array('isUnique'),
					'message' => 'Danh mục đã tồn tại'
				),
				'notEmpty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Bạn chưa nhập trường này'
				),
            ),
			'description' => array(
				'notEmpty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Bạn chưa nhập trường này'
				),
            ),
        );
        
        public $hasMany = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
                'Accessory' => array(
			'className' => 'Accessory',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
        
        public $actsAs = array(
		'Uploader.FileValidation' => array(
			'image' => array(
				'extension' => array('gif', 'jpg', 'png', 'jpeg'),
				'type' => 'image',
				'required' => true,
			)
		),
        'Uploader.Attachment' => array(
            'image' => array(
                'tempDir' => TMP,
				'nameCallback' => 'renameUploadFile',
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'type' => 'image',
				'required' => true,
                'finalPath' => '',
                'mimeType' => array('image/gif'),
                'quality' => 100,
                'transforms' => array(
                    'thumb_50' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 50,
                        'height' => 50,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_408' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 408,
                        'height' => 308,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_218' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 218,
                        'height' => 137,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                )
            )
        )
    );

}
