<?php
App::uses('AppModel', 'Model');
/**
 * Contact Model
 *
 */
class Contact extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'contact';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
	//validate
	public $validate = array(
		'email' => array(
			'rule' => 'email',
			'message' => 'Mail bạn nhập chưa đúng'
		),
		'name' => array(
			'rule'=> array('notEmpty'),
			'message' => 'Bạn chưa nhập tên'
		),
		'phone' => array(
			'rule' => array('numeric'),
			'message' => 'Điện thoại chỉ được nhập số'
		),
		'content' => array(
			'rule' => 'notEmpty',
			'message' => 'Bạn chưa nhập yêu cầu'
		)
	);

}
