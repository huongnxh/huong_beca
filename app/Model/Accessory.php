<?php
App::uses('AppModel', 'Model');
/**
 * Accessory Model
 *
 */
class Accessory extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
        public $validate = array(
            'code' => array(
                'rule' => array('isUnique'),
                'message' => 'Mã phụ kiện đã tồn tại'
            ),
            'price' => array(
                'rule' => array('notEmpty'),
                'message' => 'Bạn chưa nhập trường này'
            ),
            'name' => array(
                'rule' => array('notEmpty'),
                'message' => 'Bạn chưa nhập trường này'
            ),
			'description' => array(
                'rule' => array('notEmpty'),
                'message' => 'Bạn chưa nhập trường này'
            ),
			
        );
        
        public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        public $hasMany = array(
            'Gallery' => array(
			'className' => 'Gallery',
			'foreignKey' => 'accessory_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('id' => 'desc'),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
        );
        
        public $actsAs = array(
		'Uploader.FileValidation' => array(
			'image' => array(
				'extension' => array('gif', 'jpg', 'png', 'jpeg'),
				'type' => 'image',
				'required' => true,
			)
		),
        'Uploader.Attachment' => array(
            'image' => array(
                'tempDir' => TMP,
				'nameCallback' => 'renameUploadFile',
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'type' => 'image',
                'finalPath' => '',
                'mimeType' => array('image/gif'),
                'quality' => 100,
                'transforms' => array(
                    'thumb_50' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 50,
                        'height' => 50,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_408' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 408,
                        'height' => 308,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_1280' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 1280,
                        'height' => 854,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_218' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 218,
                        'height' => 137,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    ),
                    'thumb_95' => array(
                        'nameCallback' => 'transformNameCallback',
                        'class' => 'crop',
                        'width' => 95,
                        'height' => 72,
                        'quality' => 100,
                        'overwrite' => true,
                        'aspect' => true,
                    )
                )
            )
        )
    );

}
