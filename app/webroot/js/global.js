/**
 * Created by Haivu on 3/5/14.
 */

var OrderImage =
{
    init: function() {
        this.deleteImage();
        this.addImage();
        this.confirm();
    },

    deleteImage: function() {
        var deleteBtn = $('.deleteImage');
        deleteBtn.click(function(e) {
            $(this).parents('.imageItem').remove();
            e.preventDefault;
        })
    },

    addImage: function() {
        var addBtn = $('.addImage');
        addBtn.click(function() {
            var model = $(this).data('model');
            if (typeof model == 'undefined') {
                model = 'Gallery'
            }

            var productId = $(this).data('product-id');
            var numberInput = parseInt($('.imageItem').length) + 1;
            var nameInput = 'data[' +model+'][' + numberInput + '][image]';
            var imageItem =
                '<div class="imageItem">' +
                '<input type="file" name="' + nameInput + '" required = "required" class="fileInput">' +
                '<input type="hidden" name="data[Gallery][' + numberInput + '][product_id]" value="' + productId + '" id="Gallery' + numberInput + 'ProductId">' +
                '<a href="#" class="btn_small_blue deleteImage">Xóa</a>' +
                '<div class="height10"></div></div>';
            $(this).parents('td').find('.input').append(imageItem);
            OrderImage.deleteImage();
        })
    },

    confirm: function() {
        var nextBtn = $('.AddImage');
        nextBtn.click(function() {
            if (!$('.fileInput').val()) {
				if($('.msgRed').length){$('.msgRed').html('Bạn chưa chọn ảnh')}else{
					$('#center').prepend('<h1 class="msgRed" style="color: red">Bạn chưa chọn ảnh</h1>');
				}
                
                return false;
            }
            $('.msgRed').remove();
        });

        var prevBtn = $('.prevBtn');
        prevBtn.click(function() {
            $('.confirm').slideUp(function() {
                $('#right').slideDown();
            });
        });

        var confirmBtn = $('.confirmBtn');
        confirmBtn.click(function() {
            var searchBox = $('.search-box');
            if (searchBox.is(':hidden')) {
                searchBox.slideDown();
            } else {
                searchBox.slideUp();
            }
        })
    }
};

var Image =
{
    init:function() {
        this.delete();
    },

    delete: function() {
        var deleteBtn = $('.deleteImageProduct');
        deleteBtn.each(function() {
            $(this).click(function() {
                if (!confirm('Bạn có chắc chắn muốn xoá?')) {
                    return false;
                }
                var currentImage = $(this);
                var url = $(this).data('delete');
                $.ajax({
                    url: url,
                    dataType: 'JSON',
                    success: function(data) {
                        currentImage.remove();
                    }
                });
                return false;
            })
        })

    }
};

$(document).ready(function() {
    OrderImage.init();
    Image.init();
});
