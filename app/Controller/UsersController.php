<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $components = array('Paginator');

        public function admin_home(){
            $group = $this->User->getListGroup();
            $this->set('group',$group);
        }
        
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
                $groups = $this->User->getListGroup();
                $this->set(compact('groups'));
	}

        public function login() {
            // Check is login => redirect
            $userKey = $this->Session->read('UserInfo');

            if(isset($userKey))
            {
                // redirect
                //$this->login_redirect($user);
                //debug($userKey);die;
            }
            // ----------------

            $this->layout = null;
            if($this->request->is('post')) 
            {
                $username = $this->request->data['User']['username'];
                $password = Security::hash($this->request->data['User']['password'], 'sha1', true);            
                $user = $this->User->find('first',array('conditions'=>array('User.username'=>$username,'User.password'=>$password)));
                        
                if(!empty($user))
                {
                    if($user['User']['active'])
                    {
                        // if 'remember me' checked, save cookie
                        if(isset($this->request->data['MemberLogin']) && $this->request->data['MemberLogin'] == true)
                        {
                            if(isset($this->request->data['User']['remember'])){$this->Cookie->write('MemberKey', $loginKey, null, '30 days');}
                            else{$this->Cookie->delete('MemberKey');}
                            $this->Session->write('MemberInfo', $user['User']);
                        }
                        else
                        {
                            if(isset($this->request->data['User']['remember'])){$this->Cookie->write('UserKey', $loginKey, null, '30 days');}
                            else{$this->Cookie->delete('UserKey');}
                            $this->Session->write('UserInfo', $user['User']);
                        }

                        // Member redirect to previous page
                        $memberRedirectUrl = $this->Session->read('MemberRedirectUrl');
                        if(isset($memberRedirectUrl))
                        {
                            $this->Session->delete('MemberRedirectUrl');
                            $this->redirect($memberRedirectUrl);
                        }

                        // Redirect base on group

                        if($user['User']['group'] == 1)
                        {      
                            $this->redirect(array('controller'=>'users','action'=>'home','admin'=>true));
                        }
                        else if($user['User']['group'] == 2)
                        {
                            $this->redirect(array('controller'=>'users','action'=>'index','admin'=>false));
                        }
                        else
                        {
                            debug('Config redirect here!');
                        }
                    }
                    else
                    {
                        if(isset($this->request->data['MemberLogin']) && $this->request->data['MemberLogin'] == true)
                        {
                            $this->flashErrorLogin(__('MsgUserDisactive', true));
                        }
                        else
                        {
                            $this->flashError(__('MsgUserDisactive', true));
                        }

                        $this->redirect($this->referer());
                    }
                }
                else
                {
                    if(isset($this->request->data['MemberLogin']) && $this->request->data['MemberLogin'] == true)
                    {
                        $this->flashErrorLogin(__('MsgLoginFail', true));
                    }
                    else
                    {
                        $this->flashError(__('MsgLoginFail', true));
                    }

                    $this->redirect($this->referer());
                }
            }
        }
        
/**
 * logout method
 */    
        public function logout() 
        {
                $this->Session->delete('UserInfo');
                $this->Cookie->delete('UserKey');
                $this->redirect(array('controller'=>'users','action'=>'login','admin'=>0));
        }
        
/**
 * admin_profile_password method
 */
        public function admin_profile_password() {
           
            if (!empty($this->request->data)) {
                $this->User->set($this->request->data);
                $this->User->id = $this->curUser['id'];
                if ($this->User->validates()) {
                    $this->User->save();
                    $this->flashSuccess(__('MsgPasswordChanged', true));
                    $this->redirect(array('action' => 'profile_password'));
                }
            }
        }    
        
/**
 * admin_reset_password method
 */ 
       public function admin_reset_password($id = null) {
            if (!$this->User->exists($id)) {
                throw new NotFoundException(__('Invalid user'));
            }else{
                if (!empty($this->request->data)) {
                    $this->User->set($this->request->data);
                    $this->User->id = $id;
                    if ($this->User->validates()) {
                        $this->User->save();
                        $this->flashSuccess(__('MsgPasswordChanged', true));
                        $this->redirect(array('action' => 'index'));
                    }
                }
                $options = array('conditions'=>array('User.' . $this->User->primaryKey => $id));
                $this->request->data = $this->User->find('first', $options);
            }
       }

       /**
 * admin_profile_edit method
 */        
        public function admin_profile() {
            if($this->request->is('post') || $this->request->is('put')){
                if ($this->User->save($this->request->data)) {
                    $this->flashSuccess(__('MsgUserSaved', true));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->flashError(__('MsgUserNotSaved', true));
                }
            }else{
                $options = array('conditions'=>array('User.' . $this->User->primaryKey => $this->curUser['id']));
                $this->request->data = $this->User->find('first', $options);
            }
        }
        
	

	public function admin_add() {
            $groups = $this->User->getListGroup();
            $this->set('groups', $groups);
		if ($this->request->is('post')) {
                        if ($this->User->save($this->request->data)) {
                                $this->flashSuccess(__('MsgUserSave', true));
                                return $this->redirect(array('action' => 'index'));
                        } else {
                                //$this->flashError(__('The user could not be saved. Please, try again.'));
                        }
                    }
	}
        
        public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
                    if ($this->User->save($this->request->data)) {
                        $this->flashSuccess(__('MsgUserSaved', true));
                        return $this->redirect(array('action' => 'index'));
                    } else {
                        $this->flashError(__('MsgUserNotSaved', true));
                    }
		} else {
                    $groups = $this->User->getListGroup();
                    $this->set('groups', $groups);
                    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                    $this->request->data = $this->User->find('first', $options);
		}
	}


	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->flashSuccess(__('The user has been deleted.', true));
		} else {
			$this->flashError(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
