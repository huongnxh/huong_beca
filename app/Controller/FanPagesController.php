<?php
App::uses('AppController', 'Controller');
/**
 * FanPages Controller
 *
 * @property FanPage $FanPage
 * @property PaginatorComponent $Paginator
 */
class FanPagesController extends AppController {

	public $components = array('Paginator');
        
        public function admin_index() {
            $this->FanPage->recursive = 0;
            $this->set('fanPages', $this->Paginator->paginate());
        }
        
        public function admin_edit($id = null){
            if (!$this->User->exists($id)) {
				throw new NotFoundException(__('Invalid user'));
			}
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->FanPage->id = $id;
				if ($this->FanPage->save($this->request->data)) {
					$this->flashSuccess(__('MsgUserSaved', true));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->flashError(__('MsgUserNotSaved', true));
				}
			} else {
				$options = array('conditions' => array('FanPage.' . $this->FanPage->primaryKey => $id));
				$this->request->data = $this->FanPage->find('first', $options);
			}
        }

}
