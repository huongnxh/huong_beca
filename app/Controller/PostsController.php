<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {


	public $components = array('Paginator');


	public function news() {
		$this->Post->recursive = 0;
                $this->Paginator->settings = array(
                    'conditions'=>array('Post.post_category_id'=>TIN_TUC, 'Post.active'=>1),
                    'limit' => 5,
					'order' => array('Post.created'=>'desc')
                );
		$this->set(array(
					'code' => 'new',
                    'news' => $this->Paginator->paginate(),
                    'title_for_layout' => 'Tin tức'
                ));
	}
        
        public function view($id = null){
            if (!$this->Post->exists($id)) {
                throw new NotFoundException(__('Invalid post'));
            }
            $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
            $post = $this->Post->find('first', $options);
            $category_post = $post['Post']['post_category_id'];
			$code = null;
			switch($category_post){
				case KIEN_THUC_BE_CA:
					$code = 'kienthuc';
					break;
				case TIN_TUC:
					$code = 'new';
					break;
				case DICH_VU:
					$code = 'services';
					break;
			}
            $this->set(array(
				'code' => $code,
                'post' => $post,
                'title_for_layout' => $post['Post']['title'],
                'recent' => $this->Post->find('all', array('conditions'=>array('Post.'. $this->Post->primaryKey.' !=' => $id, 'post_category_id'=>$category_post), 'limit'=>5, 'order'=>array('Post.created'=>'desc')))
            ));
        }
        
        public function kien_thuc_be_ca() {
            $this->Post->recursive = 0;
            $this->Paginator->settings = array(
                'conditions'=>array('Post.post_category_id'=>KIEN_THUC_BE_CA, 'Post.active'=>1),
                'limit' => 5,
				'order' => array('Post.created'=>'desc')
            );
            $news = $this->Paginator->paginate();
            $this->set(array(
				'code' => 'kienthuc',
                'news' => $news,
                'title_for_layout' => 'Kiến thức bể cá'
            ));
        }
        
        public function services() {
            $this->Post->recursive = 0;
            $this->Paginator->settings = array(
                'conditions'=>array('Post.post_category_id'=>DICH_VU, 'Post.active'=>1),
                'limit' => 5,
				'order' => array('Post.created'=>'desc')
            );
            $this->set(array(
				'code' => 'services',
                'news' => $this->Paginator->paginate(),
                'title_for_layout' => 'Dịch dụ'  
            ));
        }

        public function about() {
            $this->set(array(
				'code' => 'about',
                'about' => $this->Post->find('first',
					array('conditions'=>array('Post.post_category_id'=>GIOI_THIEU, 'Post.active'=>1),
					'order' => array('Post.created' => 'DESC')
				)),
                'title_for_layout' => 'Giới thiệu'
            ));
	}

	public function admin_index() {
		$this->Post->recursive = 0;
                $this->paginate = array(
                    'order'=>array('Post.display_order'=>'asc', 'Post.created'=>'desc')
                );
		$this->set('posts', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Post->create();
                        $this->Post->id = $id;
			if ($this->Post->save($this->request->data)) {
				$this->flashSuccess(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The post could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->Post->findById($id);
                }
	}

	public function admin_delete($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->Post->delete()) {
			$this->flashSuccess(__('The post has been deleted.'));
		} else {
			$this->flashError(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
}
