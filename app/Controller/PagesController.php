<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Category', 'Accessory', 'Product', 'Post', 'Gallery','GalleryGroup');


        /**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
        
        public function home(){
            $this->Category->recursive = 2;
            $this->Category->hasMany['Product']['limit'] = 1;
            $this->set('categories', $this->Category->find('all', array(
                'conditions'=>array(
                    'Category.display' => 1,
					'Category.type' => SAN_PHAM
                ),
                'order'=>array('Category.display_order'=>'asc'),
				'limit' => 12
            )));
			
			$this->set('accessories', $this->Category->find('all', array(
				'conditions' => array(
					'Category.type' => PHU_KIEN,
					'Category.display' => 1,
				),
				'limit' => 4,
				'order' => array('Category.display_order'=>'asc')
			)));
            $this->set(array(
				'title_for_layout' => 'Bể cá thái hà',
				'code' => 'home'
			));
            
            #set accessory
            //$this->set_accessory();
            #set galleryGroup
            $this->set_list_gallery();
            #set news
            $this->set_news();
        }
}
