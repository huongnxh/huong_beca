<?php
App::uses('AppController', 'Controller');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 */
class ContactsController extends AppController {

	public $components = array('Paginator');
        
        public function admin_index() {
            $this->Contact->recursive = 0;
            $this->paginate = array(
                'order' => array('Contact.created'=>'desc')
            );
            $this->set('contacts', $this->Paginator->paginate());
        }
        
        public function add() {
            if($this->request->is('post')){
                $this->Contact->create();
                $this->Contact->set($this->request->data);
                if($this->Contact->save()){
                    $this->flashSuccess(__('Cảm ơn bạn đã gửi phản hồi cho chúng tôi!'));
                    $this->redirect(array('controller' => 'contacts', 'action' => 'add'));
                }  else {
                    //$this->flashError(__('Error!'));
                }   
            }
            $this->set(array('title_for_layout' => 'Liên hệ', 'code'=>'contact'));
        }
        
        public function admin_delete($id = null) {
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		if ($this->Contact->delete()) {
			$this->flashSuccess(__('The gallery has been deleted.'));
		} else {
			$this->flashError(__('The gallery could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
