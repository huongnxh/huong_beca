<?php
App::uses('AppController', 'Controller');
/**
 * Videos Controller
 *
 * @property Video $Video
 * @property PaginatorComponent $Paginator
 */
class VideosController extends AppController {

	public $components = array('Paginator');

	public function admin_index() {
		$this->Video->recursive = 0;
		$this->paginate = array(
			'order' => array(
				'Video.id' => 'desc'
			)
		);
		$this->set('videos', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Video->exists($id)) {
			throw new NotFoundException(__('Invalid video'));
		}
		$options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
		$this->set('video', $this->Video->find('first', $options));
	}

	public function admin_add($id = null) {
                $this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Video->create();
                        $this->Video->id = $id;
			if ($this->Video->save($this->request->data)) {
				$this->flashSuccess(__('The video has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The video could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->Video->findById($id);
                }
	}


	public function admin_delete($id = null) {
		$this->Video->id = $id;
		if (!$this->Video->exists()) {
			throw new NotFoundException(__('Invalid video'));
		}
		if ($this->Video->delete()) {
			$this->flashSuccess(__('The video has been deleted.'));
		} else {
			$this->flashError(__('The video could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function view($id = null) {
		if (!$this->Video->exists($id)) {
			throw new NotFoundException(__('Invalid video'));
		}
		$options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
		$this->set('video', $this->Video->find('first', $options));
                $this->Paginator->settings = array(
					'conditions' => array('Video.active'=>1, 'Video.id !=' => $id),
                    'limit' => 12,
                );
                $this->set(array('videos' => $this->Paginator->paginate(), 'code' => 'home'));
	}
}
