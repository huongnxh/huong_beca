<?php
App::uses('AppController', 'Controller');
/**
 * GalleryGroups Controller
 *
 * @property GalleryGroup $GalleryGroup
 * @property PaginatorComponent $Paginator
 */
class GalleryGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		#set group_gallery_list for index
		$this->set_group_gallery_content();
		$this->loadModel('Video');
		$video = $this->Video->find('all', array('conditions'=>array('Video.active'=>1)));
		$this->set(array('videos' => $video, 'code' => 'home'));
	}
        
	public function admin_index() {
		$this->GalleryGroup->recursive = 0;
		$this->paginate = array(
			'order' => array('GalleryGroup.id' => 'desc')
		);
		$this->set('galleryGroups', $this->Paginator->paginate());
	}

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post')||$this->request->is('put')) {
			$this->GalleryGroup->create();
                        $this->GalleryGroup->id = $id;
			if ($this->GalleryGroup->save($this->request->data)) {
				$this->flashSuccess(__('The gallery group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The gallery group could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->GalleryGroup->findById($id);
                }
	}
	public function admin_delete($id = null) {
		$this->GalleryGroup->id = $id;
		if (!$this->GalleryGroup->exists()) {
			throw new NotFoundException(__('Invalid gallery group'));
		}
		if ($this->GalleryGroup->delete()) {
			$this->flashSuccess(__('The gallery group has been deleted.'));
		} else {
			$this->flashError(__('The gallery group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
