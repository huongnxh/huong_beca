<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

	public $components = array('Paginator');
        
	public function admin_index() {
            $this->Category->recursive = 0;
            $this->paginate = array(
                'order' => array('Category.display_order'=>'asc', 'Category.id'=>'desc')
            );  
            $this->set('categories', $this->Paginator->paginate());
	}

	public function admin_add($id = null) {
			$this->set('id', $id);
            if ($this->request->is('post') || $this->request->is('put'))  {
                $this->Category->create();
                $this->Category->id = $id;
                if ($this->Category->save($this->request->data)) {
                    $this->flashSuccess(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    //$this->flashError(__('The category could not be saved. Please, try again.'));
                }
            }
            if($id){
                $this->request->data = $this->Category->findById($id);
            }
	}

	public function admin_delete($id = null) {
            $this->Category->id = $id;
            if (!$this->Category->exists()) {
                throw new NotFoundException(__('Invalid category'));
            }
            if ($this->Category->delete()) {
                $this->flashSuccess(__('The category has been deleted.'));
            } else {
                $this->flashError(__('The category could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
	}
        
        


        #view product of category with $id
        public function view_product($id = null){
            $this->loadModel('Product');
            $product_categories = $this->Product->find('all', array('conditions'=>array('Product.category_id'=>$id), 'recursive'=> -1));
            $this->set('product_categories', $product_categories);
            debug($product_categories);die;
        }
        
        
}
