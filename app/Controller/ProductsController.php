<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class ProductsController extends AppController {

	public $components = array('Paginator');
        public $uses = array('Product', 'Accessory',  'Gallery');
        public $helpers = array('Paginator');

        public function admin_index() {
		$this->Product->recursive = 0;
		$this->paginate = array(
			'order' => array('Product.id' => 'desc')
		);
		$this->set('products', $this->Paginator->paginate('Product'));
	} 

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Product->create();
                        $this->Product->id = $id;
			if ($this->Product->save($this->request->data)) {
				$this->flashSuccess(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The product could not be saved. Please, try again.'));
			}
		}
                $categories = $this->Product->Category->find(
					'list', 
					array(
						'conditions' => array(
							'Category.type' => SAN_PHAM
						),
					)
				);
                $this->set('categories', $categories);
                # Nếu có $id thì edit
                if($id){
                    $this->request->data = $this->Product->findById($id);
                }
	}

	public function admin_delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->Product->delete()) {
			$this->flashSuccess(__('The product has been deleted.'));
		} else {
			$this->flashError(__('The product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        #index
        public function index($id = null) {
				$this->Product->recursive = 0;
                //$this->set('accessories', $this->Accessory->find('all'));
                $this->Paginator->settings = array('limit'=>12);
                #Nếu có $id tìm những sản phẩm có category_id = $id
				$accessories = null;
                if($id){
                    $this->Paginator->settings = array(
                        'limit'=>12,
                        'conditions'=> array('Product.category_id'=>$id, 'Product.active'=>1),
                    );
                }else{
					$accessories = $this->Accessory->find('all', array('conditions'=>array('Accessory.active'=>1),'limit'=>4));
				}
                $products = $this->Paginator->paginate('Product', array('Product.active'=>1));
                $this->set(array(
					'active_id' => '',
					'category_id' => $id,
					'code' => 'product',
                    'products' => $products,
                    'accessories'=> $accessories,
                    'title_for_layout' => 'Trang Sản Phẩm',
                ));
                
                #set gallery
                $this->set_list_gallery();
                #set news
                $this->set_news();
	} 
        #view product
        public function view($id = null) {
            if(!$this->Product->exists($id)){
                throw new NotFoundException(__('Invalid product'));
            }
            $product = $this->Product->findById($id);
            $this->set(array(
				'active_id' => '',
				'category_id' =>  $product['Product']['category_id'],
				'code' => 'product',
                'product' => $product,
                'title_for_layout' => $product['Product']['name']
            ));
                        
            #sản phẩm liên quan
            $this->loadModel('Product');
            $options = $this->Product->find('first', array('conditions'=>array('Product.'.$this->Product->primaryKey=>$id),'fields'=>'category_id'));
            $categoryID = $options['Product']['category_id'];
            $this->set('products', $this->Product->find('all', array('conditions'=>array('Product.'.$this->Product->primaryKey .' !='=> $id, 'Product.active'=>1, 'Product.category_id'=>$categoryID), 'limit'=>12)));
            #set gallery
            $this->set_list_gallery();
            #set news
            $this->set_news();
        }
}
