<?php
App::uses('AppController', 'Controller');
/**
 * Galleries Controller
 *
 * @property Gallery $Gallery
 * @property PaginatorComponent $Paginator
 */
class GalleriesController extends AppController {

	public $components = array('Paginator');

        public function index($id = null) {
		#set group_gallery_list for index
		$this->set_group_gallery_content($id);
		$this->Gallery->recursive = 0;
		$this->set('galleries', $this->Gallery->find('all', array('conditions'=>array('Gallery.gallery_group_id'=>$id, 'Gallery.active'=>1))));
		$this->set('code', 'home');
	}

        
	public function admin_index() {
		$this->Gallery->recursive = 0;
		$this->paginate = array(
				'conditions'=>array('Gallery.gallery_group_id !=' => 0),
				'order' => array('Gallery.id' => 'desc')
			);
		$this->set('galleries', $this->Paginator->paginate());
	}

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Gallery->create();
                        $this->Gallery->id = $id;
			if ($this->Gallery->save($this->request->data)) {
				$this->flashSuccess(__('The gallery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The gallery could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->Gallery->findById($id);
                }
                $galleryGroups = $this->Gallery->GalleryGroup->find('list');
                //debug($galleryGroups);die;
                $this->set(compact('galleryGroups'));
	}

	public function admin_delete($id = null) {
		$this->Gallery->id = $id;
		if (!$this->Gallery->exists()) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		if ($this->Gallery->delete()) {
			$this->flashSuccess(__('The gallery has been deleted.'));
		} else {
			$this->flashError(__('The gallery could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function admin_product_image($id = null) {
            if (!$id) {
                $this->Session->setFlash(__('Not isset'),'default', array('class' => 'message-error'));
                $this->redirect(array('action' => 'index'));
            }

            if ($this->request->is('post')) {
                if ($this->Gallery->saveMany($this->request->data['Gallery'])) {
                    $this->Session->setFlash(__('Save successful'));
                };
            }

            $productImages = $this->Gallery->find('all', array(
                'conditions' => array(
                        array('product_id' => $id)
                ),
                'fields' => array(
                    'id', 'image'
                )
            ));
            $this->set('productImages', $productImages);
            $this->set('product_id', $id);
        }
        
        public function admin_accessory_image($id = null) {
            if (!$id) {
                $this->Session->setFlash(__('Not isset'),'default', array('class' => 'message-error'));
                $this->redirect(array('action' => 'index'));
            }

            if ($this->request->is('post')) {
                if ($this->Gallery->saveMany($this->request->data['Gallery'])) {
                    $this->Session->setFlash(__('Save successful'));
                };
            }

            $productImages = $this->Gallery->find('all', array(
                'conditions' => array(
                        array('accessory_id'=>$id)
                ),
                'fields' => array(
                    'id', 'image'
                )
            ));
            $this->set('productImages', $productImages);
            $this->set('product_id', $id);
        }
        public function admin_gallery_delete($id) {
            $this->autoRender = false;
            if ($this->Gallery->delete($id)) {
                return json_encode(array(
                    'status' => 'success',
                ));
            };
    }
               
        
}
