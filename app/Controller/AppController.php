<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('Helper', 'Time');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array('Cookie','Session', 'Paginator', 'DebugKit.Toolbar');
    public $uses = array('User', 'Gallery', 'Category', 'FanPage', 'Product', 'Support', 'Count', 'UserOnline');
    public $curUser;
    public $curMember;


    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->set_counter();
        $this->visit();
        $this->online();
        if (strpos($this->action, 'admin_') !== false)
        {
            $this->layout = 'admin';
            $this->checkAdminLogin();
        }
        else
        {
            $this->checkMemberLogin();
        }
        #set support
        $this->set_support();
        #set menuCategory
        $this->menuCategory();
        #set fangage
        $this->set('fanpage', $this->FanPage->find('first'));
		#set gallery_list_left
		$this->set_list_gallery_left();
    }
    
    /* Check user is login or not */
    public function checkAdminLogin()
    {
        $userInfo = $this->Session->read('UserInfo');
        
        if(isset($userInfo))
        {
            $this->curUser = $userInfo;
        }
        else
        {
            $userKey = $this->Cookie->read('UserKey');
            if(isset($userKey))
            {
                $user = $this->User->find('first',array('conditions'=>array('User.login_key'=>$userKey)));
                if(!empty($user))
                {
                    $this->curUser = $user['User'];
                }
                else
                {
                    //$this->Session->write('RedirectUrl',$_SERVER['REDIRECT_URL']);
                    $this->redirect(array('controller'=>'users','action'=>'login','admin'=>0));
                }
            }
            else
            {
                //$this->Session->write('RedirectUrl',$_SERVER['REDIRECT_URL']);
                $this->redirect(array('controller'=>'users','action'=>'login','admin'=>0));
            }
        }
        
        // => Is Login
        $this->getTopMenu();
        $this->set('curUser',$this->curUser);
        $this->User->curUser = $this->curUser;

    }
    
    public function checkMemberLogin()
    {
        $memberInfo = $this->Session->read('MemberInfo');
        
        if(isset($memberInfo))
        {
            $this->curMember = $memberInfo;
        }
        else
        {
            $memberKey = $this->Cookie->read('MemberKey');
            if(isset($memberKey))
            {
                $user = $this->User->find('first',array('conditions'=>array('User.login_key'=>$memberKey)));
                if(!empty($user))
                {
                    $this->curMember = $user['User'];
                }
                else
                {
                    if (strpos($this->action, 'member_') !== false)
                    {
                        //$this->Session->write('RedirectUrl',$_SERVER['REDIRECT_URL']);
                        $this->redirect(array('controller'=>'pages','action'=>'index','member'=>0));
                    }
                }
            }
            else
            {
                if (strpos($this->action, 'member_') !== false)
                {
                    //$this->Session->write('RedirectUrl',$_SERVER['REDIRECT_URL']);
                    $this->redirect(array('controller'=>'pages','action'=>'index','member'=>0));
                }
            }
        }
        // => Is Login
        $this->getTopMenuMember();
        $this->set('curMember',$this->curMember);
    }  
    
    public function getTopMenu(){
        $this->loadModel('Menu');
        $listParent = $this->Menu->find('all', array('conditions'=>array('Menu.group'=>$this->curUser['group'], 'Menu.parent_id'=>null, 'Menu.display'=>true), 'order'=>array('Menu.display_order')));
        $topMenu = array();
        foreach ($listParent as $item) {
            $parentID = $item['Menu']['id'];
            $listChild = $this->Menu->find('all', array('conditions'=>array('Menu.parent_id'=>$parentID), 'Menu.display' => true, 'order'=>array('Menu.display_order')));
            array_push($topMenu, array('MainMenu'=>$item, 'SubMenu'=>$listChild));
        }
        $this->set('topMenu',$topMenu);
    }
    
    public function getTopMenuMember(){
        $this->loadModel('Menu');
        $listParent = $this->Menu->find('all', array('conditions'=>array('Menu.group'=>2, 'Menu.parent_id'=>null, 'Menu.display'=>true), 'order'=>array('Menu.display_order')));
        $topMenu = array();
        foreach ($listParent as $item) {
            //$parentID = $item['Menu']['id'];
            //$listChild = $this->Menu->find('all', array('conditions'=>array('Menu.parent_id'=>$parentID), 'Menu.display' => true, 'order'=>array('Menu.display_order')));
            //array_push($topMenu, array('MainMenu'=>$item, 'SubMenu'=>$listChild));
            array_push($topMenu, array('MainMenu'=>$item));
        }
        $this->set('topMenuMember',$topMenu);
    }
    
    public function menuCategory() {
        $this->set('menus', $this->Category->find('all', array('conditions'=>array('Category.display'=>1))));
    }
    
    # sets up successful session flash message for view
    public function flashSuccess($msg, $url = null)
    {
        $this->Session->setFlash(__($msg), 'flash'.DS.'success');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }

    # sets up warning session flash message for view
    public function flashWarning($msg, $url = null)
    {
        $this->Session->setFlash(__($msg), 'flash'.DS.'warning');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }

    # sets up information session flash message for view
    public function flashInfo($msg, $url = null)
    {
        $this->Session->setFlash(__($msg), 'flash'.DS.'info');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }

    # sets up successful session flash message for view
    public function flashError($msg, $url = null)
    {
        $this->Session->setFlash(__($msg), 'flash'.DS.'error');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }
    
    public function flashErrorLogin($msg, $url = null)
    {
        $this->Session->setFlash(__($msg), 'flash'.DS.'error_login');
        if (!empty($url))
        {
            $this->redirect($url, null, true);
        }
    }
    
    public function webroot() 
    {
        // Fix unknown bug
        $url = $_SERVER['REQUEST_URI'];
        $url = 'http://'.$_SERVER['SERVER_NAME'].str_replace('/app/webroot','',$url);
        $this->redirect($url);
    }
    
    function admin_set_active($id,$bool = true)
    {
        
        $model = $this->modelClass;
        $this->$model->id = $id;
        if (!$this->$model->exists() || $id == 1) 
        {
            $this->redirect(array('action'=>'index'));
        }
        
        $this->$model->saveField('active',$bool);
        $this->redirect($this->referer());
        
    }
    
    function admin_set_new($id,$bool = true)
    {
        $model = $this->modelClass;
        $this->$model->id = $id;
        if (!$this->$model->exists()) 
        {
            $this->redirect(array('action'=>'index'));
        }
        
        $this->$model->saveField('is_new',$bool);
        $this->redirect($this->referer());
    }
    
    function admin_set_display($id,$bool = true)
    {
        $model = $this->modelClass;
        $this->$model->id = $id;
        if (!$this->$model->exists()) 
        {
            $this->redirect(array('action'=>'index'));
        }
        
        $this->$model->saveField('display',$bool);
        $this->redirect($this->referer());
    }
    
    function admin_set_display_price($id,$bool = true)
    {
        $model = $this->modelClass;
        $this->$model->id = $id;
        if (!$this->$model->exists()) 
        {
            $this->redirect(array('action'=>'index'));
        }
        
        $this->$model->saveField('display_price',$bool);
        $this->redirect($this->referer());
    }
    
    function admin_sort()
        {
            $model = $this->modelClass;
            //debug($this->request->data['Order']);die;
            if(!empty($this->request->data))
            {
                $data = array();
                $i = 1;
                foreach($this->request->data['Order'] as $id)
                {
                    $row = array
                    (
                        'id'=>$id,
                        'display_order'=>$i
                    );
                    array_push($data,$row);
                    ++$i;
                }
                //debug($data);die;
//                return json_encode(array(
//                    'status' => 'success',
//                    'text' => 'abc'
//                ));
                if($this->$model->saveAll($data))
                {
                    $this->flashSuccess('MsgSortParentSuccessful');
                }
                $this->redirect($this->referer());
            }
        }
        
		//Set galleryGroup for gallery right
        public function set_list_gallery(){
            #set gallery
            $this->loadModel('GalleryGroup');
			$gallery = $this->Gallery->find('all',
				array(
					'conditions'=>array('Gallery.active'=>1),
					'group' => 'Gallery.gallery_group_id',
					'limit' => 9,
					'order' => array('GalleryGroup.id'=>'desc')
				)
			);
            $this->set('galleryGroups', $gallery);
        }
		
		//Set galleryGroup for gallery index
		public function set_group_gallery_content($id = null){
			$groupId = null;
			if($id){
				$groupId = $id;
			}
			$this->GalleryGroup->recursive = 1;
			$this->loadModel('Gallery');
			$this->Paginator->settings = array(
				'Gallery' => array(
					'conditions' => array('Gallery.active' => 1, 'GalleryGroup.id !=' => $groupId),
					'group' => 'Gallery.gallery_group_id',
					'order' => array('GalleryGroup.id'=>'desc'),
					'limit' => 12
				),
			);
			$gallery = $this->Paginator->paginate('Gallery');
			$this->set('galleryGroupsContent', $gallery);
		}
		
		//Set galleryGroup for sidebar left
		public function set_list_gallery_left(){
            #set gallery left
            $this->loadModel('GalleryGroup');
			
			$this->loadModel('Gallery');
			//$gallery = $this->GalleryGroup->find('all', array('limit'=>6, 'order'=>array('GalleryGroup.id'=>'desc')));
			$gallery = $this->Gallery->find('all',
				array(
					'conditions'=>array('Gallery.active'=>1),
					'group' => 'Gallery.gallery_group_id',
					'limit' => 6,
					'order' => array('GalleryGroup.id'=>'desc')
				)
			);
			$this->set('galleryGroupsLeft', $gallery);
		}
        
        public function set_news() {
            #set news
            $this->loadModel('Post');
            $this->set('news', $this->Post->find('all', array('conditions'=>array('Post.active'=>1, 'Post.post_category_id'=>TIN_TUC), 'limit'=>4, 'order'=>array('Post.created'=>'desc'))));
        }
        
        public function set_accessory() {
            $this->loadModel('Category');
            $this->set('accessories',$this->Category->find('all', array(
                'conditons'=>array(
                    'Category.type' => PHU_KIEN
                )
            )));
        }
        
        public function set_support(){
            $this->set('supports', $this->Support->find('all', array('conditions'=>array('Support.active'=>1))));
        }
        
        #Đếm số lượng online
        function online() {
            $online_session_id = $this->Session->id();
            if(empty($online_session_id)) return ;

            $this->loadModel('UserOnline');
            $user_online = $this->UserOnline->findByIpClient($online_session_id);
            $time_out = time() + 900;
            
//            $this->UserOnline->deleteAll();
//            $user_online_new = $this->UserOnline->create();
//            $user_online_new['ip_client'] = $online_session_id;
//            $user_online_new['time_in'] = date('Y-m-d H:i:s',time());
//            $user_online_new['time_out'] = $time_out;
//            $this->UserOnline->save($user_online_new);
            
            
            
            if(empty($user_online) || $user_online == false) {
                //$this->UserOnline->deleteAll(array('UserOnline.time_out <=' => time()) , false  , false);
                $user_online_new = $this->UserOnline->create();
                $user_online_new['ip_client'] = $online_session_id;
                $user_online_new['time_in'] = date('Y-m-d H:i:s',time());
                $user_online_new['time_out'] = $time_out;
                $this->UserOnline->save($user_online_new);
            }
            else {
                $this->UserOnline->deleteAll(array('UserOnline.ip_client' => $online_session_id) , false  , false);
                $user_online['UserOnline']['time_out'] = $time_out;
                $this->UserOnline->save($user_online);
            }
            $this->UserOnline->deleteAll(array('UserOnline.time_out <=' => time()) , false  , false);
        }
        
        #visit
        public function visit() {
            $timeHelper = new TimeHelper(new View(null));
            $time = new \DateTime();
            $visited = $this->Count->find('first', array(
                'conditions' => array(
                    $timeHelper->dayAsSql($time, 'created')
                )
            ));
            
            if ($visited) {
                $visited['Count']['count'] += 1;
                $visited['Count']['total'] += 1;
            } else {
                $yesterdayVisited = $this->Count->find('first', array('order' => array('Count.created ASC')));
                $visited['Count']['count'] = 1;
                if ($yesterdayVisited) {
                    $visited['Count']['total'] = $yesterdayVisited['Count']['total'] + 1;
                } else {
                    $visited['Count']['total'] = 1;
                }
            }
            $this->Count->set($visited);
            $this->Count->save();
            $yesterday = new \DateTime('yesterday');
            $array = get_object_vars($yesterday);
            $count = $this->Count->find('count');
            //debug($count);die;
            if($count >= 3){
                $this->Count->deleteAll(array('Count.created <' =>$array['date']) , false  , false);
            }
        }
        
        public function set_counter() {
            #set so nguoi dang online
            $soluong = $this->UserOnline->find('count');
            if (!empty($this->params['requested'])) {
               return $soluong;
            } 
            else {
               $this->set(compact('soluong'));
            }
            #set tong so truy cap
            $this->set('tong_so_truy_cap',$this->Count->find('first', array('order' => array('Count.created DESC'))));
            #set so truy cap ngay hom qua
            //$this->set('yesTotal',  $this->Count->find('first', array('order' => array('Count.created ASC'))));
            
            $timeHelper = new TimeHelper(new View(null));
            $time = new \DateTime('yesterday');
            $this->set('yesTotal',  $this->Count->find('first', array(
                        'conditions' => array(
                            $timeHelper->dayAsSql($time, 'created')
                        )
                    )));
        }
        
        
}
