<?php
App::uses('AppController', 'Controller');
/**
 * Accessories Controller
 *
 * @property Accessory $Accessory
 * @property PaginatorComponent $Paginator
 */
class AccessoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($id = null) {
                #set Accessory
                //$this->set_accessory();
                #set gallery
                $this->set_list_gallery();
                #set news
                $this->set_news();
                #Nếu có $id tìm những sản phẩm có category_id = $id
                if($id){
                    $this->Paginator->settings = array(
                        'limit'=>12,
                        'conditions'=>array('Accessory.category_id'=>$id, 'Accessory.active'=>1),
                    );
                    $this->set(array(
						'category_id' => '',
						'active_id' => $id,
						'code' => 'product',
                        'accessories' => $this->Paginator->paginate(),
                        'title_for_layout' => 'Trang phụ kiện'
                    ));
                }  else {
                    $this->flashError(__('NoData'));
                }
	}
        
        public function view($id = null) {
		if (!$this->Accessory->exists($id)) {
			throw new NotFoundException(__('Invalid accessory'));
		}
		$options = array('conditions' => array('Accessory.' . $this->Accessory->primaryKey => $id));
                $accessory = $this->Accessory->find('first', $options);
                $this->set(array(
					'category_id' => '',
					'active_id' => $accessory['Accessory']['category_id'],
					'code' => 'product', 
                    'accessory' => $accessory,
                    'accessories' => $this->Accessory->find('all', array('conditions'=>array('Accessory.'.$this->Accessory->primaryKey .' !='=> $id, 'Accessory.active'=>1), 'limit'=>12)),
                    'title_for_layout' => $accessory['Accessory']['name']
                ));
                #set gallery
                $this->set_list_gallery();
                #set news
                $this->set_news();
	}
	public function admin_index() {
		$this->Accessory->recursive = 0;
		$this->paginate = array(
			'order' => array(
				'Accessory.id' => 'desc'
			)
		);
		$this->set('accessories', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Accessory->exists($id)) {
			throw new NotFoundException(__('Invalid accessory'));
		}
		$options = array('conditions' => array('Accessory.' . $this->Accessory->primaryKey => $id));
		$this->set('accessory', $this->Accessory->find('first', $options));
	}

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Accessory->create();                    
                        $this->Accessory->id = $id;
			if ($this->Accessory->save($this->request->data)) {
				$this->flashSuccess(__('The accessory has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The accessory could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->Accessory->findById($id);
                }
                $categories = $this->Product->Category->find('list', array('conditions'=>array('Category.type'=>PHU_KIEN)));
                $this->set('categories', $categories);
	}

	public function admin_delete($id = null) {
		$this->Accessory->id = $id;
		if (!$this->Accessory->exists()) {
			throw new NotFoundException(__('Invalid accessory'));
		}
		if ($this->Accessory->delete()) {
			$this->flashSuccess(__('The accessory has been deleted.'));
		} else {
			$this->flashError(__('The accessory could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
}
