<?php
App::uses('AppController', 'Controller');
/**
 * Supports Controller
 *
 * @property Support $Support
 * @property PaginatorComponent $Paginator
 */
class SupportsController extends AppController {

	public $components = array('Paginator');

	public function admin_index() {
		$this->Support->recursive = 0;
		$this->set('supports', $this->Paginator->paginate());
	}

	public function admin_add($id = null) {
		$this->set('id', $id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Support->create();
                        $this->Support->id = $id;
			if ($this->Support->save($this->request->data)) {
				$this->flashSuccess(__('The support has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->flashError(__('The support could not be saved. Please, try again.'));
			}
		}
                if($id){
                    $this->request->data = $this->Support->findById($id);
                }
	}

	public function admin_delete($id = null) {
		$this->Support->id = $id;
		if (!$this->Support->exists()) {
			throw new NotFoundException(__('Invalid support'));
		}
		if ($this->Support->delete()) {
			$this->flashSuccess(__('The support has been deleted.'));
		} else {
			$this->flashError(__('The support could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
}
