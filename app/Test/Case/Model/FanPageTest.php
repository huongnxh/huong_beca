<?php
App::uses('FanPage', 'Model');

/**
 * FanPage Test Case
 *
 */
class FanPageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fan_page'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FanPage = ClassRegistry::init('FanPage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FanPage);

		parent::tearDown();
	}

}
