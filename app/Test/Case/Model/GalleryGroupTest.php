<?php
App::uses('GalleryGroup', 'Model');

/**
 * GalleryGroup Test Case
 *
 */
class GalleryGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.gallery_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GalleryGroup = ClassRegistry::init('GalleryGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GalleryGroup);

		parent::tearDown();
	}

}
