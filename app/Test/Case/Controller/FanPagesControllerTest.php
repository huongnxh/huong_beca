<?php
App::uses('FanPagesController', 'Controller');

/**
 * FanPagesController Test Case
 *
 */
class FanPagesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fan_page',
		'app.user'
	);

}
